package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.BuildBattleUtil.getSection;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class removeCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "remove";
    }

    @Override
    public String getDescription() {
        return "Retirer des points";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle remove <box> <pts>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 4) {
            String first = args[0];
            String second = args[1];
            String third = args[2];
            String four = args[3];
            if (first.equalsIgnoreCase("remove")) {
                if (player.hasPermission(getPermission())) {
                    int torem = 0;
                    try {
                        torem = Integer.parseInt(third);
                    } catch (NumberFormatException e) {
                        msg(player, "&9&6" + third + " n'est pas un chiffre valide.");
                        return;
                    }
                    (Score.getInstance()).configurations.getBuildBattle().remove(second, torem);
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 4) {
            String first = args[0];
            String second = args[1];
            String third = args[2];
            String four = args[3];
            if (first.equalsIgnoreCase("remove")) {
                int torem = 0;
                try {
                    torem = Integer.parseInt(third);
                } catch (NumberFormatException e) {
                    log("&9&6" + third + " n'est pas un chiffre valide.");
                }
                (Score.getInstance()).configurations.getBuildBattle().remove(second, torem);
            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            return getSection();
        }

        return arg;
    }
}
