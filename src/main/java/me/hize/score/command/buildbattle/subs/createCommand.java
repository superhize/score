package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class createCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "create";
    }

    @Override
    public String getDescription() {
        return "Créer une section";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle create <section>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if
        (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("create")) {
                if (player.hasPermission(getPermission())){
                    if ((Score.getInstance()).configurations.getBuildBattle().getConfig().isConfigurationSection(second)) {
                        msg(player, "&9&cExiste déjà");
                    } else {
                        ConfigurationSection section = (Score.getInstance()).configurations.getBuildBattle().getConfig().createSection(second);
                        section.set("points", Integer.valueOf(0));
                        section.set("owner", new String());
                        section.set("vote.genial", new LinkedList());
                        section.set("vote.bien", new LinkedList());
                        section.set("vote.bof", new LinkedList());
                        section.set("vote.nul", new LinkedList());
                        (Score.getInstance()).configurations.getBuildBattle().saveConfig();
                        msg(player, "&9&cOk");
                    }
                }else
                    msg(player, getNoPerm());

            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("create")) {
                if ((Score.getInstance()).configurations.getBuildBattle().getConfig().isConfigurationSection(second)) {
                    log("existe déjà");
                } else {
                    ConfigurationSection section = (Score.getInstance()).configurations.getBuildBattle().getConfig().createSection(second);
                    section.set("points", Integer.valueOf(0));
                    section.set("owner", new String());
                    section.set("vote.genial", new LinkedList());
                    section.set("vote.bien", new LinkedList());
                    section.set("vote.bof", new LinkedList());
                    section.set("vote.nul", new LinkedList());
                    (Score.getInstance()).configurations.getBuildBattle().saveConfig();
                }
            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
