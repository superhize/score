package me.hize.score.command.buildbattle.subs;

import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.BuildBattleUtil.ltheme;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class themevote2Command extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "themevote2";
    }

    @Override
    public String getDescription() {
        return "Définir le thème 2";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle themevote2 <theme>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("themevote2")) {
                if (player.hasPermission(getPermission())) {
                    Bukkit.getServer().dispatchCommand((CommandSender)player, "hd setline bbvote2 3 &a" + second);
                    ltheme.put("theme2", second);
                    msg(player, "&9&aThème changé.");
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("themevote2")) {
                Bukkit.getServer().dispatchCommand(console, "hd setline bbvote2 3 &a" + second);
                ltheme.put("theme2", second);
                log("&9&aThème changé.");
            }
        }else
            log(getSyntax());
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
