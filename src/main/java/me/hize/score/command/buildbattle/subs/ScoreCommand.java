package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;
import static me.hize.score.command.buildbattle.BuildBattleCommand.sortByValue;

public class ScoreCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "score";
    }

    @Override
    public String getDescription() {
        return "Afficher le score";
    }

    @Override
    public String getSyntax() {
        return "/buildbatle score";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            String first = args[0];
            if (first.equalsIgnoreCase("score")) {
                if (player.hasPermission(getPermission())) {
                    Map<String, Object> map = (Score.getInstance()).configurations.getBuildBattle().getConfig().getValues(false);
                    msg(player, "");
                    Map<String, Integer> m = new HashMap<>();
                    for (Map.Entry<String, Object> e : map.entrySet()) {
                        String section = (String)e.getKey();
                        if (!section.equalsIgnoreCase("players"))
                            if (!(Score.getInstance()).configurations.getBuildBattle().getConfig().getString(section + ".owner").isEmpty()) {
                                String owner = (String)(Score.getInstance()).configurations.getBuildBattle().getConfig().get(section + ".owner");
                                int i = (Score.getInstance()).configurations.getBuildBattle().getConfig().getInt(section + ".points");
                                m.put(section + ":" + owner, Integer.valueOf(i));
                            }
                    }
                    Map<String, Integer> a = sortByValue(m);
                    for (Map.Entry<String, Integer> e : a.entrySet()) {
                        String casename = ((String)e.getKey()).split(":")[0];
                        String owner = ((String)e.getKey()).split(":")[1];
                        int i = ((Integer)e.getValue()).intValue();
                        msg(player, "&9&5[&c" + casename + "&5] &a" + owner + " &9: &6" + i);
                    }
                    msg(player, "");
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {

        if (args.length == 1) {
            String first = args[0];
            if (first.equalsIgnoreCase("score")) {
                Map<String, Object> map = (Score.getInstance()).configurations.getBuildBattle().getConfig().getValues(false);
                Map<String, Integer> m = new HashMap<>();
                for (Map.Entry<String, Object> e : map.entrySet()) {
                    String section = (String)e.getKey();
                    if (!section.equalsIgnoreCase("players"))
                        if (!(Score.getInstance()).configurations.getBuildBattle().getConfig().getString(section + ".owner").isEmpty()) {
                            String owner = (String)(Score.getInstance()).configurations.getBuildBattle().getConfig().get(section + ".owner");
                            int i = (Score.getInstance()).configurations.getBuildBattle().getConfig().getInt(section + ".points");
                            m.put(section + ":" + owner, Integer.valueOf(i));
                        }
                }
                Map<String, Integer> a = sortByValue(m);
                for (Map.Entry<String, Integer> e : a.entrySet()) {
                    String casename = ((String)e.getKey()).split(":")[0];
                    String owner = ((String)e.getKey()).split(":")[1];
                    int i = ((Integer)e.getValue()).intValue();
                    log("&9&5[&c" + casename + "&5] &a" + owner + " &9: &6" + i);
                }
            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
