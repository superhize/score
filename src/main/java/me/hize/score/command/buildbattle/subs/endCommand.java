package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.ScoreUtil.brodcast;
import static me.hize.score.ScoreUtil.msg;
import static me.hize.score.command.buildbattle.BuildBattleCommand.sortByValue;

public class endCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "end";
    }

    @Override
    public String getDescription() {
        return "Terminer l'event";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle end";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==1){
            if (player.hasPermission(getPermission())) {
                Map<String, Object> map = (Score.getInstance()).configurations.getBuildBattle().getConfig().getValues(false);
                msg(player, "");
                Map<String, Integer> m = new HashMap<>();
                for (Map.Entry<String, Object> e : map.entrySet()) {
                    String section = (String)e.getKey();
                    if (!section.equalsIgnoreCase("players"))
                        if (!(Score.getInstance()).configurations.getBuildBattle().getConfig().getString(section + ".owner").isEmpty()) {
                            String owner = (String)(Score.getInstance()).configurations.getBuildBattle().getConfig().get(section + ".owner");
                            int i = (Score.getInstance()).configurations.getBuildBattle().getConfig().getInt(section + ".points");
                            m.put(section + ":" + owner, Integer.valueOf(i));
                        }
                }
                Map<String, Integer> a = sortByValue(m);

                brodcast("");
                brodcast("&e======================================");
                brodcast("&9»     &6Event terminé.");
                brodcast("");
                brodcast("&9»     Classement: ");
                for (Map.Entry<String, Integer> e : a.entrySet()) {
                    String casename = ((String)e.getKey()).split(":")[0];
                    String owner = ((String)e.getKey()).split(":")[1];
                    int i = ((Integer)e.getValue()).intValue();
                    msg(player, "&9» &5[&c" + casename + "&5] &a" + owner + " &9: &6" + i+" points");

                }
                brodcast("&e======================================");
                brodcast("");
                (Score.getInstance()).configurations.getBuildBattle().reset();
            }else
                msg(player, getNoPerm());
        }
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
