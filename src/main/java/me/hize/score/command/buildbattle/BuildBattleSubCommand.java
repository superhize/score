package me.hize.score.command.buildbattle;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public abstract class BuildBattleSubCommand {

    public abstract String getPermission();

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getSyntax();

    public abstract void perform(Player player, String args[]);

    public abstract void consolePerform(ConsoleCommandSender console, String args[]);

    public abstract List<String> getSubcommandArgs(Player player, String args[]);


}
