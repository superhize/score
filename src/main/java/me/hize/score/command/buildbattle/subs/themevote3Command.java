package me.hize.score.command.buildbattle.subs;

import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.BuildBattleUtil.ltheme;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class themevote3Command extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "themevote3";
    }

    @Override
    public String getDescription() {
        return "Définir le thème 3";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle themevote3 <theme>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("themevote3")) {
                if (player.hasPermission(getPermission())) {
                    Bukkit.getServer().dispatchCommand((CommandSender)player, "hd setline bbvote3 3 &a" + second);
                    ltheme.put("theme3", second);
                    msg(player, "&9&aThème changé.");
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("themevote3")) {
                Bukkit.getServer().dispatchCommand(console, "hd setline bbvote3 3 &a" + second);
                ltheme.put("theme3", second);
                log("&9&aThème changé.");
            }
        }else
            log(getSyntax());
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
