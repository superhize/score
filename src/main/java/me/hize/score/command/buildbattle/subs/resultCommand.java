package me.hize.score.command.buildbattle.subs;

import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.Random;

import static me.hize.score.BuildBattleUtil.*;
import static me.hize.score.BuildBattleUtil.vote;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class resultCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "result";
    }

    @Override
    public String getDescription() {
        return "Résultats des votes";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle result";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            String first = args[0];
                if (first.equalsIgnoreCase("result")) {
                    if (player.hasPermission(getPermission())) {
                        msg(player, "");
                        StringBuilder sb = new StringBuilder();
                        for (String s : getMostVote(vote))
                            sb.append((String)ltheme.get(s) + ", ");
                        String res = sb.toString();
                        String[] r = res.split(", ");
                        int r2 = r.length;
                        int random = (new Random()).nextInt(r2);
                        String allo = res.replace(r[random], "&c" + r[random] + "&a");
                        msg(player, "&9&bThème gagnant: &a" + allo);
                        msg(player, "");
                        for (Map.Entry<String, Integer> e : vote.entrySet()) {
                            String theme = e.getKey();
                            int nbvote = ((Integer)e.getValue()).intValue();
                            msg(player, "&9&6" + (String)ltheme.get(theme) + ": &a" + nbvote);
                        }
                        msg(player, "");
                    }else
                        msg(player, getNoPerm());
                }

        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 2) {
            String first = args[0];
                if (first.equalsIgnoreCase("result")) {
                    StringBuilder sb = new StringBuilder();
                    for (String s : getMostVote(vote))
                        sb.append((String)ltheme.get(s) + ", ");
                    String res = sb.toString();
                    String[] r = res.split(", ");
                    int r2 = r.length;
                    int random = (new Random()).nextInt(r2);
                    String allo = res.replace(r[random], "&c" + r[random] + "&a");
                    System.out.print("&9&bThgagnant: &a" + allo);
                    for (Map.Entry<String, Integer> e : vote.entrySet()) {
                        String theme = e.getKey();
                        int nbvote = ((Integer)e.getValue()).intValue();
                        log("&9&6" + (String)ltheme.get(theme) + ": &a" + nbvote);
                    }
                }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
