package me.hize.score.command.buildbattle.subs;

import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static me.hize.score.BuildBattleUtil.*;
import static me.hize.score.ScoreUtil.msg;

public class voteCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return "";
    }

    @Override
    public String getName() {
        return "vote";
    }

    @Override
    public String getDescription() {
        return "Votez !";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle vote <1/2/3/4>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("vote")) {
                    int i;
                    try {
                        i = Integer.parseInt(second);
                    } catch (NumberFormatException e) {
                        msg(player, "&9&6Théme non valide. Utilisez /buildbattle vote <1/2/3/4>");
                        return;
                    }
                    switch (i) {
                        case 1:
                            if (!hasvoted1.containsKey(player.getName())) {
                                hasvoted1.put(player.getName(), Boolean.valueOf(true));
                                vote.put("theme1", Integer.valueOf(vote.containsKey("theme1") ? (((Integer)vote.get("theme1")).intValue() + 1) : 1));
                                msg(player, "&9&aVote enregistré");
                                break;
                            }
                            msg(player, "&9&cVous avez déjà voté pour ce thème");
                            break;
                        case 2:
                            if (!hasvoted2.containsKey(player.getName())) {
                                hasvoted2.put(player.getName(), Boolean.valueOf(true));
                                vote.put("theme2", Integer.valueOf(vote.containsKey("theme2") ? (((Integer)vote.get("theme2")).intValue() + 1) : 1));
                                msg(player, "&9&aVote enregistré");
                                break;
                            }
                            msg(player, "&9&cVous avez déjà voté pour ce thème");
                            break;
                        case 3:
                            if (!hasvoted3.containsKey(player.getName())) {
                                hasvoted3.put(player.getName(), Boolean.valueOf(true));
                                vote.put("theme3", Integer.valueOf(vote.containsKey("theme3") ? (((Integer)vote.get("theme3")).intValue() + 1) : 1));
                                msg(player, "&9&aVote enregistré");
                                break;
                            }
                            msg(player, "&9&cVous avez déjà voté pour ce thème");
                            break;
                        case 4:
                            if (!hasvoted4.containsKey(player.getName())) {
                                hasvoted4.put(player.getName(), Boolean.valueOf(true));
                                vote.put("theme4", Integer.valueOf(vote.containsKey("theme4") ? (((Integer)vote.get("theme4")).intValue() + 1) : 1));
                                msg(player, "&9&aVote enregistré");
                                break;
                            }
                            msg(player, "&9&cVous avez déjà voté pour ce thème");
                            break;
                    }

            }
        }
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            arg.add("1");
            arg.add("2");
            arg.add("3");
            arg.add("4");
        }


        return arg;
    }
}
