package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.ScoreUtil.msg;

public class statsCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "stats";
    }

    @Override
    public String getDescription() {
        return "Afficher les stats";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle stats";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            String first = args[0];
            if (first.equalsIgnoreCase("stats")) {
                if (player.hasPermission(getPermission())) {
                    msg(player, "");
                    for (String part : (Score.getInstance()).configurations.getBuildBattle().getConfig().getConfigurationSection("players").getKeys(false)) {
                        ConfigurationSection section = (Score.getInstance()).configurations.getBuildBattle().getConfig().getConfigurationSection("players");
                        int genial = section.getInt(part + ".genial");
                        int bien = section.getInt(part + ".bien");
                        int bof = section.getInt(part + ".bof");
                        int nul = section.getInt(part + ".nul");
                        msg(player, "&9&5[&6" + part + "&5] &9| &2G"+ genial + ", &aBien:" + bien + ", &cBof:" + bof + ", &4Nul:" + nul + "");
                    }
                    msg(player, "");
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 1) {
            String first = args[0];
            if (first.equalsIgnoreCase("stats")) {
                for (String part : (Score.getInstance()).configurations.getBuildBattle().getConfig().getConfigurationSection("players").getKeys(false)) {
                    ConfigurationSection section = (Score.getInstance()).configurations.getBuildBattle().getConfig().getConfigurationSection("players");
                    int genial = section.getInt(part + ".genial");
                    int bien = section.getInt(part + ".bien");
                    int bof = section.getInt(part + ".bof");
                    int nul = section.getInt(part + ".nul");
                    System.out.println("&9&5[&6" + part + "&5] &9| &2G"+ genial + ", &aBien:" + bien + ", &cBof:" + bof + ", &4Nul:" + nul + "");
                }
            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
