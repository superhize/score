package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.BuildBattleUtil.getSection;
import static me.hize.score.ScoreUtil.msg;

public class linkCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "link";
    }

    @Override
    public String getDescription() {
        return "Lier un box à un joueur";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle link <box> <pseudo>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 3) {
            String first = args[0];
            String second = args[1];
            String third = args[2];
            if (first.equalsIgnoreCase("link")){
                if (player.hasPermission(getPermission()))
                    (Score.getInstance()).configurations.getBuildBattle().link(third, second, (CommandSender)player);
                else
                    msg(player, getNoPerm());
            }

        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 3) {
            String first = args[0];
            String second = args[1];
            String third = args[2];
            if (first.equalsIgnoreCase("link"))
                (Score.getInstance()).configurations.getBuildBattle().link(third, second, (CommandSender) Bukkit.getConsoleSender());
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            return getSection();
        }

        return arg;
    }
}
