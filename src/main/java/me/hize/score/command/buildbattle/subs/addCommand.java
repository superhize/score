package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.BuildBattleUtil.getSection;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class addCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "add";
    }

    @Override
    public String getDescription() {
        return "Ajouter des points";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle add <box> <point> <mec qui vote>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 4) {
            String first = args[0];
            String second = args[1];
            String third = args[2];
            String four = args[3];
            if (first.equalsIgnoreCase("add")) {
                if (player.hasPermission(getPermission())) {
                    int toadd = 0;
                    try {
                        toadd = Integer.parseInt(third);
                    } catch (NumberFormatException e) {
                        msg(player, "&9&6" + third + " n'est pas un chiffre valide.");
                        return;
                    }
                    (Score.getInstance()).configurations.getBuildBattle().add(second, toadd, four, (CommandSender)player);
                }
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 4) {
            String first = args[0];
            String second = args[1];
            String third = args[2];
            String four = args[3];
            if (first.equalsIgnoreCase("add")) {
                int toadd = 0;
                try {
                    toadd = Integer.parseInt(third);
                } catch (NumberFormatException e) {
                    System.out.print("&9&6" + third + " n'est pas un chiffre valide.");
                }
                (Score.getInstance()).configurations.getBuildBattle().add(second, toadd, four, (CommandSender) Bukkit.getConsoleSender());
            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 1){
            return getSection();
        }

        return arg;
    }
}
