package me.hize.score.command.buildbattle.subs;

import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.BuildBattleUtil.ltheme;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class themevote1Command extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "themevote1";
    }

    @Override
    public String getDescription() {
        return "Définir le thème 1";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle themevote1 <theme>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("themevote1")) {
                if (player.hasPermission(getPermission())) {
                    Bukkit.getServer().dispatchCommand((CommandSender)player, "hd setline bbvote1 3 &a" + second);
                    ltheme.put("theme1", second);
                    msg(player, "&9&aThème changé.");
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 2) {
            String first = args[0];
            String second = args[1];
            if (first.equalsIgnoreCase("themevote1")) {
                    Bukkit.getServer().dispatchCommand(console, "hd setline bbvote1 3 &a" + second);
                    ltheme.put("theme1", second);
                    log("&9&aThème changé.");
            }
        }else
            log(getSyntax());
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
