package me.hize.score.command.buildbattle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import me.hize.score.Score;
import me.hize.score.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

@SuppressWarnings("all")
public class BuildBattleCommand implements CommandExecutor {
    public static Map<String, Boolean> hasvoted1 = new HashMap<>();

    public static Map<String, Boolean> hasvoted2 = new HashMap<>();

    public static Map<String, Boolean> hasvoted3 = new HashMap<>();

    public static Map<String, Boolean> hasvoted4 = new HashMap<>();

    public static Map<String, Integer> vote = new HashMap<>();

    public static Map<String, String> ltheme = new HashMap<>();

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {}
        else {
            if (args.length == 0)
                return false;
            if (args.length == 1) { } else
                if (args.length == 2) {} else
                    if (args.length == 3) {
                String first = args[0];
                String second = args[1];
                String third = args[2];
                if (first.equalsIgnoreCase("link"))
                    (Score.getInstance()).configurations.getBuildBattle().link(third, second, (CommandSender)Bukkit.getConsoleSender());
            } else if (args.length == 4) {
                String first = args[0];
                String second = args[1];
                String third = args[2];
                String four = args[3];
                if (first.equalsIgnoreCase("add")) {
                    int toadd = 0;
                    try {
                        toadd = Integer.parseInt(third);
                    } catch (NumberFormatException e) {
                        System.out.print("&9&6" + third + " n'est pas un chiffre valide.");
                    }
                    (Score.getInstance()).configurations.getBuildBattle().add(second, toadd, four, (CommandSender)Bukkit.getConsoleSender());
                } else if (first.equalsIgnoreCase("remove")) {
                    int torem = 0;
                    try {
                        torem = Integer.parseInt(third);
                    } catch (NumberFormatException e) {
                        System.out.print("&9&6" + third + " n'est pas un chiffre valide.");
                    }
                    (Score.getInstance()).configurations.getBuildBattle().remove(second, torem);
                }
            }
        }
        return false;
    }

    public void help(Player p) {
        msg(p, "");
        msg(p, "&6==== &bBuildBattle &6====");
        msg(p, "&9&6/buildbattle &d<add/remove> <nom> <points>");
        msg(p, "&9&6/buildbattle &3link &d<nom> <pseudo>");
        msg(p, "&9&6/buildbattle &3reset");
        msg(p, "&9&6/buildbattle &3score");
        msg(p, "&6==== &bBuildBattle &6====");
        msg(p, "");
    }

    public void msg(Player p, String m) {
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', m));
    }

    public static Map<String, Integer> sortByValue(Map<String, Integer> map) {
        Map<String, Integer> sorted = (Map<String, Integer>)map.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, java.util.LinkedHashMap::new));
        return sorted;
    }

    public static List<String> getMostVote(Map<String, Integer> map) {
        List<String> resultList = new ArrayList<>();
        int currentMaxValuevalue = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (((Integer)entry.getValue()).intValue() > currentMaxValuevalue) {
                resultList.clear();
                resultList.add(entry.getKey());
                currentMaxValuevalue = ((Integer)entry.getValue()).intValue();
                continue;
            }
            if (((Integer)entry.getValue()).intValue() == currentMaxValuevalue)
                resultList.add(entry.getKey());
        }
        return resultList;
    }
}
