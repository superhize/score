package me.hize.score.command.buildbattle;

import me.hize.score.command.buildbattle.subs.*;
import me.hize.score.command.checkpoint.CheckpointSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;
import static me.hize.score.command.buildbattle.subs.helpCommand.displayBuildBattleHelp;

public class BuildBattleCommandManager implements TabExecutor {

    private static ArrayList<BuildBattleSubCommand> subCommands = new ArrayList<>();

    public BuildBattleCommandManager() {

        subCommands.add(new addCommand());
        subCommands.add(new createCommand());
        subCommands.add(new linkCommand());
        subCommands.add(new removeCommand());
        subCommands.add(new resetCommand());
        subCommands.add(new ScoreCommand());
        subCommands.add(new statsCommand());
        subCommands.add(new themevote1Command());
        subCommands.add(new themevote2Command());
        subCommands.add(new themevote3Command());
        subCommands.add(new themevote4Command());
        subCommands.add(new voteCommand());
        subCommands.add(new helpCommand());
        subCommands.add(new resultCommand());
        subCommands.add(new endCommand());

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = null;

        if (sender instanceof Player)
            p = (Player) sender;

        if (p != null) {
            if (args.length > 0) {
                for (int i = 0; i < getBuildBattleSubCommands().size(); i++) {
                    if (args[0].equalsIgnoreCase(getBuildBattleSubCommands().get(i).getName())) {
                        if (sender instanceof Player)
                            getBuildBattleSubCommands().get(i).perform(p, args);
                        else
                            getBuildBattleSubCommands().get(i).consolePerform(Bukkit.getServer().getConsoleSender(), args);
                    }
                }
            } else if (args.length == 0) {
                if (sender instanceof Player)
                    displayBuildBattleHelp(p, 1);
                else
                    log("------------------------------------");
                for (int i = 0; i < getBuildBattleSubCommands().size(); i++) {
                    if (!(sender instanceof Player))
                        log(getBuildBattleSubCommands().get(i).getSyntax() + " - " + getBuildBattleSubCommands().get(i).getDescription());
                }
                if (!(sender instanceof Player))
                    log("------------------------------------");
            }
        } else {
            if (p != null) {
                msg(p, "  &9»  &6Plugin d'event crée par &cHiZe_");
            }
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {

        if (args.length == 1) {
            ArrayList<String> subCommandArg = new ArrayList<>();

            for (int i = 0; i < getBuildBattleSubCommands((Player)sender).size(); i++) {
                subCommandArg.add(getBuildBattleSubCommands((Player)sender).get(i).getName());
            }
            return subCommandArg;
        } else if (args.length >= 2) {
            for (int i = 0; i < getBuildBattleSubCommands((Player)sender).size(); i++) {
                if (args[0].equalsIgnoreCase(getBuildBattleSubCommands((Player)sender).get(i).getName())) {
                    return getBuildBattleSubCommands((Player)sender).get(i).getSubcommandArgs((Player) sender, args);
                }
            }
        }
        return null;
    }

    public static ArrayList<BuildBattleSubCommand> getBuildBattleSubCommands() {
        return subCommands;
    }

    public static ArrayList<BuildBattleSubCommand> getBuildBattleSubCommands(Player p) {
        ArrayList<BuildBattleSubCommand> list = new ArrayList<>();

        for (int i = 0; i < getBuildBattleSubCommands().size(); i++) {
            if (p.hasPermission(getBuildBattleSubCommands().get(i).getPermission()) || getBuildBattleSubCommands().get(i).getPermission() == "") {
                list.add(getBuildBattleSubCommands().get(i));
            }
        }

        return list;
    }

}
