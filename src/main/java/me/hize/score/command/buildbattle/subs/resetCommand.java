package me.hize.score.command.buildbattle.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.buildbattle.BuildBattleSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.BuildBattleUtil.getNoPerm;
import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;

public class resetCommand extends BuildBattleSubCommand {
    @Override
    public String getPermission() {
        return Util.PERM;
    }

    @Override
    public String getName() {
        return "reset";
    }

    @Override
    public String getDescription() {
        return "Reset event";
    }

    @Override
    public String getSyntax() {
        return "/buildbattle reset";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            String first = args[0];
            if (first.equalsIgnoreCase("reset")) {
                if (player.hasPermission(getPermission())) {
                    (Score.getInstance()).configurations.getBuildBattle().reset();
                    msg(player, "&9&6Reset > &a Succès");
                }else
                    msg(player, getNoPerm());
            }
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 1) {
            String first = args[0];
            if (first.equalsIgnoreCase("reset")) {
                (Score.getInstance()).configurations.getBuildBattle().reset();
                log("&9&6Reset > &a Succès");
            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
