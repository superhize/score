package me.hize.score.command.checkpoint.subs;

import me.hize.score.Score;
import me.hize.score.command.checkpoint.CheckpointSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.msg;

public class TpCommand extends CheckpointSubCommand {
    @Override
    public String getName() {
        return "tp";
    }

    @Override
    public String getDescription() {
        return "Tp to your checkpoint";
    }

    @Override
    public String getSyntax() {
        return "/checkpoint tp";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (player.getWorld().getName().equals(Score.getInstance().getConfig().get("globalworld"))) {
            if(args.length==1){
                if ((Score.getInstance()).configurations.getCheckPoints().getCheckPoint().get(player.getName()) != null) {
                    String[] format = (Score.getInstance()).configurations.getCheckPoints().getCheckPoint().getString(player.getName()).split(",");
                    String world = format[0];
                    double x = Double.parseDouble(format[1]);
                    double y = Double.parseDouble(format[2]);
                    double z = Double.parseDouble(format[3]);
                    float yaw = Float.parseFloat(format[4]);
                    float pitch = Float.parseFloat(format[5]);
                    Location loca = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
                    msg(player, "&9&aTéléportation vers le checkpoint.");
                    player.teleport(loca);
                }
            }
        }else {
            msg(player, "&9&cVous ne pouvez pas utiliser cette commande ici.");
        }


    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        logError("La console ne peut pas faire ça, voyons.");
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {


        return null;
    }
}
