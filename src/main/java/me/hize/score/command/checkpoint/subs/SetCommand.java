package me.hize.score.command.checkpoint.subs;

import me.hize.score.Score;
import me.hize.score.command.checkpoint.CheckpointSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.msg;

public class SetCommand extends CheckpointSubCommand {
    @Override
    public String getName() {
        return "set";
    }

    @Override
    public String getDescription() {
        return "Set checkpoint";
    }

    @Override
    public String getSyntax() {
        return "/checkpoint set";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (player.getWorld().getName().equals(Score.getInstance().getConfig().get("globalworld"))) {
            if(args.length == 1){
                String world = player.getWorld().getName();
                double x = player.getLocation().getX();
                double y = player.getLocation().getY();
                double z = player.getLocation().getZ();
                float yaw = player.getLocation().getYaw();
                float pitch = player.getLocation().getPitch();
                String format = world + "," + x + "," + y + "," + z + "," + yaw + "," + pitch;
                (Score.getInstance()).configurations.getCheckPoints().addCheckpoint(player.getName(), format);
                (Score.getInstance()).configurations.getCheckPoints().saveConfig();
                msg(player, "&9&cCheckpoint défini.");
            }else
                msg(player, getSyntax());
        }else {
            msg(player, "&9&cVous ne pouvez pas utiliser cette commande ici.");
        }


    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        logError("La console ne peut pas faire ça, voyons.");
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {


        return null;
    }
}
