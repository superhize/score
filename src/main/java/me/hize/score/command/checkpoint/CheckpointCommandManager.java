package me.hize.score.command.checkpoint;

import me.hize.score.command.checkpoint.subs.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.msg;
import static me.hize.score.command.checkpoint.subs.HelpCommand.displayCheckpointHelp;

public class CheckpointCommandManager implements TabExecutor {

    private static ArrayList<CheckpointSubCommand> subCommands = new ArrayList<>();

    public CheckpointCommandManager() {

        subCommands.add(new SetCommand());
        subCommands.add(new TpCommand());
        subCommands.add(new HelpCommand());
        subCommands.add(new InfoCommand());
        subCommands.add(new ViewCommand());

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = null;

        if (sender instanceof Player)
            p = (Player) sender;

        if (p != null) {
            if (args.length > 0) {
                for (int i = 0; i < getCheckpointSubCommands().size(); i++) {
                    if (args[0].equalsIgnoreCase(getCheckpointSubCommands().get(i).getName())) {
                        if (sender instanceof Player)
                            getCheckpointSubCommands().get(i).perform(p, args);
                        else
                            getCheckpointSubCommands().get(i).consolePerform(Bukkit.getServer().getConsoleSender(), args);
                    }
                }
            } else if (args.length == 0) {
                if (sender instanceof Player)
                    displayCheckpointHelp(p, 1);
                else
                    log("------------------------------------");
                for (int i = 0; i < getCheckpointSubCommands().size(); i++) {
                    if (!(sender instanceof Player))
                        log(getCheckpointSubCommands().get(i).getSyntax() + " - " + getCheckpointSubCommands().get(i).getDescription());
                }
                if (!(sender instanceof Player))
                    log("------------------------------------");
            }
        } else {
            if (p != null) {
                msg(p, "  &9»  &6Plugin d'event crée par &cHiZe_");
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {

        if (args.length == 1) {
            ArrayList<String> subCommandArg = new ArrayList<>();

            for (int i = 0; i < getCheckpointSubCommands().size(); i++) {
                subCommandArg.add(getCheckpointSubCommands().get(i).getName());
            }
            return subCommandArg;
        } else if(args.length >= 2){
            for (int i = 0; i < getCheckpointSubCommands().size(); i++) {
                if (args[0].equalsIgnoreCase(getCheckpointSubCommands().get(i).getName())) {
                    return getCheckpointSubCommands().get(i).getSubcommandArgs((Player)sender, args);
                }
            }
        }
        return null;
    }

    public static ArrayList<CheckpointSubCommand> getCheckpointSubCommands() {
        return subCommands;
    }



}
