package me.hize.score.command.checkpoint.subs;

import me.hize.score.command.checkpoint.CheckpointSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.msg;

public class InfoCommand extends CheckpointSubCommand {
    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getDescription() {
        return "Informations sur le plugin.";
    }

    @Override
    public String getSyntax() {
        return "/checkpoint info";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==1){
            msg(player, "  &9»  &6Plugin d'event par &cHiZe_");
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==1){
            log("Plugin d'event crée par HiZe_");
        }else
            logError(getSyntax());
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
