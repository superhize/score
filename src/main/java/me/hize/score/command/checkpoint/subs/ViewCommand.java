package me.hize.score.command.checkpoint.subs;

import me.hize.score.Score;
import me.hize.score.Util;
import me.hize.score.command.checkpoint.CheckpointSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.msg;
import static me.hize.score.Util.format;

public class ViewCommand extends CheckpointSubCommand {
    @Override
    public String getName() {
        return "view";
    }

    @Override
    public String getDescription() {
        return "Afficher le checkpoint d'un joueur";
    }

    @Override
    public String getSyntax() {
        return "/score view <player>";
    }

    public String getNoPerm() {
        return " &9»  &cVous n'avez pas la permission.";
    }

    public String cantDoThatHere() {
        return "&9&cVous ne pouvez pas utiliser cette commande ici.";
    }

    @Override
    public void perform(Player player, String[] args) {

        if (player.hasPermission(Util.PERM)) {
            if (player.getWorld().getName().equals(Score.getInstance().getConfig().get("globalworld"))) {
                if (args.length == 2) {
                    String pname = args[1];
                    String coord = Util.getCheckpoint(pname);
                    String send = coord.isEmpty() ? "Non trouvé" : (" " + coord);
                    String world = (send.equalsIgnoreCase("Non trouvé")) ? "Non trouvé": send.split(",")[0];
                    String x = (send.equalsIgnoreCase("Non trouvé")) ? "Non trouvé": send.split(",")[1];
                    String y = (send.equalsIgnoreCase("Non trouvé")) ? "Non trouvé": send.split(",")[2];
                    String z = (send.equalsIgnoreCase("Non trouvé")) ? "Non trouvé": send.split(",")[3];
                    for (int i = 0; i < send.split(",").length; i++){
                        Bukkit.broadcastMessage(send.split(",")[i]);
                    }
                    msg(player, "&9=== &aCheckpoint pour &6" + pname + " &9===");
                    msg(player, "  &9»  &6Monde: &a"+world);
                    msg(player, "  &9»  &6X: &a"+x);
                    msg(player, "  &9»  &6Y: &a"+y);
                    msg(player, "  &9»  &6Z: &a"+z);;
                    msg(player, "&9=== &aCheckpoint pour &6" + pname + " &9===");
                } else
                    msg(player, getSyntax());
            } else
                msg(player, cantDoThatHere());
        } else
            msg(player, getNoPerm());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 2) {
            String pname = args[1];
            String coord = Util.getCheckpoint(pname);
            String send = coord.isEmpty() ? "Non trouvé" : (" " + coord);
            log(" »  Checkpoint pour " + pname + ": " + send);

        } else
            logError(getSyntax());
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
