package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import me.hize.score.command.score.TutoHelper;
import net.md_5.bungee.api.chat.*;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;
import static me.hize.score.Util.VERSION;

public class TutoCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "tuto";
    }

    @Override
    public String getDescription() {
        return "Apprennez à utiliser le plugin &cSANS CASSER LE SERVEUR &4!!!!";
    }

    @Override
    public String getSyntax() {
        return "/score tuto";
    }

    @Override
    public void perform(Player player, String[] args) {

        if (args.length == 1)
            displayTuto(player, 1);
        else {
            int page = 0;

            try {
                page = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                msg(player, "  &9»  &c" + args[1] + " n'est pas un chiffre valide.");
                return;
            }

            displayTuto(player, page);
        }


    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        logError("La console n'a pas besoin de tuto.");
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            for (int i = 1; i < getMaxPageTuto()+1; i++){
                arg.add(String.valueOf(i));
            }
            return arg;
        }

        return null;
    }

    public void displayTuto(Player p, int page) {

        boolean before = false;
        boolean next = false;

        if (page != 1) {
            before = true;
        }
        if (page < getMaxPageTuto()) {
            next = true;
        }

        List<TutoHelper> commands = TutoHelper.getCommands();

        int pageMax = getMaxPageTuto();

        if (page > pageMax) {
            displayTuto(p, 1);
            return;
        }

        int index = 0;

        index = (page - 1) * TUTO_PER_PAGE;

        BaseComponent bf = new TextComponent("          " + colorLeft + LEFT_ARROW);
        BaseComponent nx = new TextComponent(colorRight + RIGHT_ARROW);
        BaseComponent bc = new TextComponent("");
        msg(p, "");

        if (before) {
            bf.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Précédent").create()));
            bf.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/score tuto " + (page - 1)));
        }

        bc.addExtra("   §9» §6---- §aScore " + VERSION + " §6---- §9«  (" + page + "/" + pageMax + ") ");

        if (next) {
            nx.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Suivant").create()));
            nx.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/score tuto " + (page + 1)));
        }

        p.spigot().sendMessage(bf, bc, nx);
        msg(p, "");

        for (int i = index; i < (index + TUTO_PER_PAGE); i++) {
            if (i < commands.size()) {


                BaseComponent bc2 = new TextComponent("");
                bc2.addExtra("§9» " + commands.get(i).getLabel().replace("&", "§"));
                bc2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, commands.get(i).getTooltip()));
                bc2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(commands.get(i).getTooltip()).create()));
                p.spigot().sendMessage(bc2);
            } else {
                p.sendMessage("\n");
                p.sendMessage("§c  ===- FIN -===");
                p.sendMessage("\n");
                break;
            }
        }
    }
}
