package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class CreateSectionCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "createsection";
    }

    @Override
    public String getDescription() {
        return "Créer une nouvelle seciton";
    }

    @Override
    public String getSyntax() {
        return "/score createsection <nom>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 2) {

            String s = args[1];
            if (!config.isConfigurationSection(s)) {
                ConfigurationSection section = config.createSection(s);
                section.addDefault("teleport-on-death", "0,0,0");
                section.addDefault("teleport-on-lose", new String[]{});
                section.addDefault("region", "region");
                section.addDefault("world", "world");
                section.addDefault("name", "name");
                section.addDefault("title", "title");
                section.addDefault("command-on-remove.lose", new String[]{"ch qm event &5[&d" + s + "&5] &c%player% &6a perdu une vie !"});
                section.addDefault("command-on-remove.death", new String[]{"ch qm event &5[&d" + s + "&5] &c%player% &6a est éliminé !"});
                refreshConfig();
                msg(player, " &9» &6Section crée. Utilisez /score <section> <args> <value> pour la modifier.");
            } else {
                msg(player, "  &9» &6La configuration «" + s + "» existe déjà. Utilisez /score " + s + " <args> <value> pour la modifier.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==2){
            String s = args[1];
            if (!config.isConfigurationSection(s)) {
                ConfigurationSection section = config.createSection(s);
                section.addDefault("teleport-on-death", "0,0,0");
                section.addDefault("teleport-on-lose", new String[]{});
                section.addDefault("region", "region");
                section.addDefault("world", "world");
                section.addDefault("name", "name");
                section.addDefault("title", "title");
                section.addDefault("command-on-remove.lose", new String[]{"ch qm event &5[&d" + s + "&5] &c%player% &6a perdu une vie !"});
                section.addDefault("command-on-remove.death", new String[]{"ch qm event &5[&d" + s + "&5] &c%player% &6a est éliminé !"});
                refreshConfig();
                log("» Section crée. Utilisez /score <section> <args> <value> pour la modifier.");
            } else {
                logError("» La configuration «" + s + "» existe déjà. Utilisez /score " + s + " <args> <value> pour la modifier.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
