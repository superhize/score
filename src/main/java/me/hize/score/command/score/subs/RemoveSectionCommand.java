package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class RemoveSectionCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "removesection";
    }

    @Override
    public String getDescription() {
        return "Supprimer une section";
    }

    @Override
    public String getSyntax() {
        return "/score removesection <nom>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length == 2){
            String section = args[1];
            if(config.isConfigurationSection(section))
            {
                config.set(section, null);
                refreshConfig();
                msg(player, "&9» &6Section supprimée.");
            }else
            {
                msg(player, "&9» &cLa configuration «"+section+"» n'existe pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length == 2){
            String section = args[1];
            if(config.isConfigurationSection(section))
            {
                config.set(section, null);
                refreshConfig();
                log("» Section supprimé.");
            }else
            {
                logError("» La configuration «"+section+"» n'existe pas..");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            return getSection();
        }

        return null;
    }
}
