package me.hize.score.command.score.subs;

import me.hize.score.Util;
import me.hize.score.command.score.ScoreSubCommand;
import me.tigerhix.lib.scoreboard.ScoreboardLib;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static me.hize.score.Score.*;
import static me.hize.score.ScoreUtil.*;

public class CreateboardCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "createboard";
    }

    @Override
    public String getDescription() {
        return "Créer le scoreboard";
    }

    @Override
    public String getSyntax() {
        return "/score createboard <section>";
    }

    @Override
    public void perform(Player player, String[] args) {

        if(args.length == 2){
            String second = args[1];
            if(config.isConfigurationSection(second))
            {
                String event = second;
                if(!(scoreboard != null))
                {
                    if(!list.isEmpty())
                    {
                        sclist.clear();
                        for (Map.Entry<Player, Integer> e : list.entrySet())
                        {
                            Player p = e.getKey();
                            scoreboard = ScoreboardLib.createScoreboard(p).setHandler(new me.hize.score.scoreboard.Scoreboard(event)).setUpdateInterval(2l);
                            Bukkit.getServer().broadcastMessage("Created scoreboard for "+p.getName());
                            sclist.add(scoreboard);
                        }
                        msg(player, "&9» &aScoreboard défini avec succès.");
                        Util.CURRENT_EVENT_NAME = second;
                    }
                    else
                        msg(player, "&9» &aListe des participant vide.");
                }
                else
                {
                    msg(player, "§c» Scoreboard déjà défini.");
                }
            }
            else
            {
                msg(player, "&9» &cLa configuration "+second+" n'exsite pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==2){
            String second = args[1];
            if(config.isConfigurationSection(second))
            {
                String event = second;
                if(!(scoreboard != null))
                {
                    if(!list.isEmpty())
                    {
                        sclist.clear();
                        for (Map.Entry<Player, Integer> e : list.entrySet())
                        {
                            Player p = e.getKey();
                            scoreboard = ScoreboardLib.createScoreboard(p).setHandler(new me.hize.score.scoreboard.Scoreboard(event)).setUpdateInterval(2l);
                            Bukkit.getServer().broadcastMessage("Created scoreboard for "+p.getName());
                            sclist.add(scoreboard);
                        }
                        log( "&9» &aScoreboard défini avec succès.");
                        Util.CURRENT_EVENT_NAME = second;
                    }
                    else
                        logError("&9» &aListe des participant vide.");
                }
                else
                {
                    logError( "§c» Scoreboard déjà défini.");
                }
            }
            else
            {
                logError( "» La configuration "+second+" n'exsite pas.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {

        if (args.length == 3) {
           return getSection();
        }

        return null;
    }
}
