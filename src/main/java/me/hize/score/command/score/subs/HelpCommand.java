package me.hize.score.command.score.subs;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import me.hize.score.command.score.ScoreSubCommand;
import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;
import static me.hize.score.Util.VERSION;
import static me.hize.score.command.score.ScoreCommandManager.*;

public class HelpCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Affiche la page d'aide";
    }

    @Override
    public String getSyntax() {
        return "/score help [page]";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1)
            displayScoreHelp(player, 1);
        else{
            int page = 0;

            try {
                page = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                msg(player, "  &9»  &6" + args[1] + " n'est pas un chiffre valide.");
                return;
            }

            displayScoreHelp(player, page);
        }

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        logError("La console n'a pas besoin d'aide.");
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            for (int i = 1; i < getScoreMaxPage()+1; i++){
                arg.add(String.valueOf(i));
            }
            return arg;
        }

        return null;
    }


    public static void displayScoreHelp(Player p, int page) {

        boolean before = false;
        boolean next = false;

        if (page != 1) {
            before = true;
        }
        if (page < getScoreMaxPage()) {
            next = true;
        }


        int pageMax = getScoreMaxPage();

        if (page > pageMax) {
            displayScoreHelp(p, 1);
            return;
        }

        int index = 0;

        index = (page - 1) * CMD_PER_PAGE;

        BaseComponent bf = new TextComponent("          " + colorLeft + LEFT_ARROW);
        BaseComponent nx = new TextComponent(colorRight + RIGHT_ARROW);
        BaseComponent bc = new TextComponent("");
        msg(p, "");

        if (before) {
            bf.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Précédent").create()));
            bf.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/score help " + (page - 1)));
        }

        bc.addExtra("   §9» §6---- §aScore "+VERSION+" §6---- §9«  (" + page + "/" + pageMax + ") ");

        if (next) {
            nx.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Suivant").create()));
            nx.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/score help " + (page + 1)));
        }

        p.spigot().sendMessage(bf, bc, nx);
        msg(p, "");

        for (int i = index; i < (index + CMD_PER_PAGE); i++) {
            if (i < getScoreSubCommands().size()) {


                BaseComponent bc2 = new TextComponent("");
                bc2.addExtra("§9» " + getScoreSubCommands().get(i).getName().replace("&", "§") + " §f- §b" + getScoreSubCommands().get(i).getDescription().replace("&", "§"));
                bc2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(getScoreSubCommands().get(i).getSyntax()).create()));
                bc2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, getScoreSubCommands().get(i).getSyntax()));
                p.spigot().sendMessage(bc2);
            } else {
                p.sendMessage("\n");
                p.sendMessage("§c  ===- FIN -===");
                p.sendMessage("\n");
                break;
            }
        }
    }
}
