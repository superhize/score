package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class ViewSectionCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "viewsection";
    }

    @Override
    public String getDescription() {
        return "Afficher les section disponible";
    }

    @Override
    public String getSyntax() {
        return "/score viewsection";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            Map<String, Object> map = config.getValues(false);
            msg(player, "");
            msg(player, "&9» &aListe des section(s):");
            for(Map.Entry<String, Object> e : map.entrySet())
            {
                String path = e.getKey();
                msg(player, "&a»   &b"+path);
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 1) {
            Map<String, Object> map = config.getValues(false);
            log( "");
            log(  "» Liste des section(s):");
            for(Map.Entry<String, Object> e : map.entrySet())
            {
                String path = e.getKey();
                log(  "»   "+path);
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
