package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class AddCmdCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "addcmd";
    }

    @Override
    public String getDescription() {
        return "Ajouter une commande éxécutée";
    }

    @Override
    public String getSyntax() {
        return "/score addcmd <onlose/ondeath> <section> <cmd>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length > 5) {
            String second = args[1];
            String third = args[2];
            refreshConfig();
            if (config.isConfigurationSection(third)) {
                ConfigurationSection section = config.getConfigurationSection(third);

                if (second.equals("onlose")) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String text = sb.toString();
                    String cmd = text.substring((text.indexOf(section.getName()) + StringUtils.length(section.getName() + 1)));

                    List<String> oldcmd = section.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                    oldcmd.add(cmd);

                    section.set(EVENT_COMMAND_ON_REMOVE_LOSE, oldcmd);
                    refreshConfig();
                    msg(player, "§9» &aCommande ajoutée.");
                } else if (second.equals("ondeath")) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String text = sb.toString();
                    String cmd = text.substring((text.indexOf(section.getName()) + StringUtils.length(section.getName()) + 1));

                    List<String> oldcmd = section.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);
                    oldcmd.add(cmd);

                    section.set(EVENT_COMMAND_ON_REMOVE_DEATH, oldcmd);
                    refreshConfig();
                    msg(player, "§9» &aCommande ajoutée.");
                } else {
                    msg(player, "&9» &aUsage: /score addcmd <section> onlose/ondeath <cmd>");
                }
            } else {
                msg(player, "&9» &cLa configuration «" + third + "» n'exsite pas.");
            }
        }else
            msg(player, getSyntax());


    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length > 5){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            if (config.isConfigurationSection(third)) {
                ConfigurationSection section = config.getConfigurationSection(third);

                if (second.equals("onlose")) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String text = sb.toString();
                    String cmd = text.substring((text.indexOf(section.getName()) + StringUtils.length(section.getName() + 1)));

                    List<String> oldcmd = section.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                    oldcmd.add(cmd);

                    section.set(EVENT_COMMAND_ON_REMOVE_LOSE, oldcmd);
                    refreshConfig();
                    log("» Commande ajoutée.");
                } else if (second.equals("ondeath")) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String text = sb.toString();
                    String cmd = text.substring((text.indexOf(section.getName()) + StringUtils.length(section.getName()) + 1));

                    List<String> oldcmd = section.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);
                    oldcmd.add(cmd);

                    section.set(EVENT_COMMAND_ON_REMOVE_DEATH, oldcmd);
                    refreshConfig();
                    log("» Commande ajouté.");
                } else {
                    log("» Usage: /score addcmd <section> <onlose/ondeath> <cmd>");
                }
            } else {
                logError("» La configuration " + third + " n'existe pas.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {

        List<String> arg = new LinkedList<>();

        if (args.length == 2) {
            arg.add("onlose");
            arg.add("ondeath");
            return arg;
        } else if (args.length == 3) {
            return getSection();
        }

        return null;
    }
}
