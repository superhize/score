package me.hize.score.command.score;

import java.util.*;

public enum TutoHelper {

    A("&60.1. &bAvant de commencer, il faut que je te dise quelques chose.",""),
    B("&60.2. &bC'est rien de très important. Quoi que...", ""),
    C("&60.3. &bTu peux passer ta souris sur une phrase pour voir apparaitre une bulle d'aide.", "§6Comme ça, tu vois, c'est cool non ?"),
    D("&60.4. &bCommencons.", "Ton chat risque d'être un peu spam, pardon"),
    E("&61.1. &bCréez la section \"knockem\"", "/score createsection knockem"),
    F("&61.2. &bDonnez lui un nom", "/score setname <nom> <section> (donnez lui le nom 'knockem' c'est important)"),
    G("&61.3. &bDonnez lui un titre",  "/score settitle <titre> <section> (vous pouvez mettre ce que vous voulez) (tu peux utiliser les couleurs)"),
    H("&61.4. &bDéfinissez la region de l'event", "/score setregion <region> <section> (ça marchera pas sinon)"),
    I("&61.5. &bDéfinissez le monde de l'event", "/score setworld <nom> <section> (ça marchera pas non plus sinon)"),
    J("&61.6. &bDéfinissez le monde global", "/score setgworld <nom> (posez pas de questions la dessuss (c'est le monde event))"),
    K("&61.6.1. &bCe que vous venez de faire, vous le faites que une fois.", "§6Sauf si le serveur explose et que faut tout recommencer"),
    L("&61.7. &bDéfinissez les commandes à éxécuter lorsqu'un joueur perd une vie", "/score addcmd onlose knockem <cmd> (pas besoin de mettre le /) (tu peux utiliser les couleurs)"),
    M("&61.8. &bDéfinissez les commandes à éxécuter lorsqu'un joueur est éliminé",  "/score addcmd ondeath knockem <cmd> (pas besoin de mettre le /) (tu peux utiliser les couleurs)"),
    N("&61.8.1. &bVous pouvez modifier les commandes par après.", "Commencez donc par faire /score viewcmd knockem"),
    O("&61.8.2. &bChaque commande à son chiffre attribué, c'est ce que vous allez utiliser.", "Faites /score removecmd onlose knockem <chiffre>"),
    P("&61.8.3. &bTu peux faire la même chose pour ondeath, c'est le même principe.", "/score removecmd ondeath knockem <chiffre>"),
    Q("&61.9 &BBien joué à toi mon ami, tu as fini la 1er partie. Tu peux maintenant passé à la 2ème partie", "clap clap clap");

    private String label;
    private String toolip;


    TutoHelper(String label, String tooltip){
        this.label = label;
        this.toolip = tooltip;
    }

    public String getLabel(){
        return this.label;
    }


    public String getTooltip()
    {
        return this.toolip;
    }

    public static List<TutoHelper> getCommands(){
        LinkedList<TutoHelper> list = new LinkedList<TutoHelper>();

        SortedMap<String, TutoHelper> map = new TreeMap<String, TutoHelper>();

        for (TutoHelper ca : TutoHelper.values()) {
            map.put(ca.getLabel(), ca);
        }


        for(TutoHelper ca : map.values()){
            list.add(ca);
        }
        return list;
    }




}