package me.hize.score.command.score.subs;

import me.hize.score.Util;
import me.hize.score.command.score.ScoreSubCommand;
import me.tigerhix.lib.scoreboard.type.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.*;
import static me.hize.score.ScoreUtil.*;

public class ToggleCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "toggle";
    }

    @Override
    public String getDescription() {
        return "Afficher le Scoreboard";
    }

    @Override
    public String getSyntax() {
        return "/score toggle <on/off>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==2){
            String second = args[1];
            if(second.equals("on"))
            {
                Iterator<Scoreboard> it = sclist.iterator();
                while(it.hasNext())
                {
                    Scoreboard sc = it.next();
                    if(sc != null && !sc.isActivated()){
                        sc.activate();
                    }

                    else
                        logError("  &9» &6Scorebaord déjà défini sur ON.");
                }
                Util.kills.put("Pingusman", 0);
                Util.spreeList.put("Pingusman", 0);
                Util.isOnSpree.put("Pingusman", false);
            }
            else if(second.equals("off"))
            {
                Iterator<Scoreboard> it = sclist.iterator();
                while(it.hasNext())
                {
                    Scoreboard scb = it.next();
                    scb.deactivate();
                    scb = null;
                }

                list.clear();
                sclist.clear();
                scoreboard=null;
                Util.kills.clear();
                Util.winners.clear();
                Util.CURRENT_EVENT_NAME = "";
                participant.clear();
                log("  &9» &6Event réinitialiser.");
            }
            else
            {
                logWarn("&9» &6/score toggle on/off.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==2){
            String second = args[1];
            if(second.equals("on"))
            {
                Iterator<Scoreboard> it = sclist.iterator();
                while(it.hasNext())
                {
                    Scoreboard sc = it.next();
                    if(sc != null && !sc.isActivated()){
                        sc.activate();
                    }

                    else
                        logError("» Scorebaord déjà défini sur ON.");
                }
                Util.kills.put("Pingusman", 0);
                Util.spreeList.put("Pingusman", 0);
                Util.isOnSpree.put("Pingusman", false);
            }
            else if(second.equals("off"))
            {
                Iterator<Scoreboard> it = sclist.iterator();
                while(it.hasNext())
                {
                    Scoreboard scb = it.next();
                    scb.deactivate();
                    scb = null;
                }

                list.clear();
                sclist.clear();
                scoreboard=null;
                Util.kills.clear();
                Util.winners.clear();
                Util.CURRENT_EVENT_NAME = "";
                participant.clear();
                log("» Event réinitialiser.");
            }
            else
            {
                logWarn("» /score toggle on/off.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
           arg.add("on");
           arg.add("off");
            return arg;
        }

        return null;
    }
}
