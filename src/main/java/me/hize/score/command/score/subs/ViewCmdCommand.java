package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class ViewCmdCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "viewcmd";
    }

    @Override
    public String getDescription() {
        return "Afficher les commandes éxécuté";
    }

    @Override
    public String getSyntax() {
        return "/score viewcmd <section>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==2){
            String second = args[1];
            String section = second;
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                List<String> cmdsonlose = sec.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                List<String> cmdsondeath = sec.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);

                msg(player, "&9» &aCommande(s) exécuté lors de la perte d'une vie ("+second+"): ");
                if(cmdsonlose.size()<1) msg(player, "&c» &aAucune.");
                for(int i=0; i<cmdsonlose.size(); i++)
                {
                    int index = i;
                    String cmd = cmdsonlose.get(index);
                    msg(player, "§9» &b["+index+"] /"+cmd);
                }

                msg(player, "");

                msg(player, "&9» &aCommande(s) exécuté lors de l'élimination ("+second+"): ");
                if(cmdsondeath.size()<1) msg(player, "&c» &aAucune.");
                for(int i=0; i<cmdsondeath.size(); i++)
                {
                    int index = i;
                    String cmd = cmdsondeath.get(index);
                    msg(player, "&9» &b["+index+"] /"+cmd);
                }
            }
            else
            {
                msg(player, "&9» &cLa configuration «"+section+"» n'existe pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==2){
            String section = args[1];
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                List<String> cmdsonlose = sec.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                List<String> cmdsondeath = sec.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);

                log("» Commande(s) exécuté lors de la perte d'une vie: ");
                if(cmdsonlose.size()<1) System.out.println("» Aucune.");
                for(int i=0; i<cmdsonlose.size(); i++)
                {
                    int index = i;
                    String cmd = cmdsonlose.get(index);
                    log("» ["+index+"] /"+cmd);
                }

                log("");

                log("» Commande(s) exécuté lors de l'élimination: ");
                if(cmdsondeath.size()<1) System.out.println("» Aucune.");
                for(int i=0; i<cmdsondeath.size(); i++)
                {
                    int index = i;
                    String cmd = cmdsondeath.get(index);
                    log("&9» &b["+index+"] /"+cmd);
                }
            }
            else
            {
                logError("» La configuration «"+section+"» n'existe pas.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            getSection();
            return arg;
        }

        return null;
    }
}
