package me.hize.score.command.score.subs;

import me.hize.score.Util;
import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

import java.util.List;
import java.util.Map;

public class SpreeCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "viewspree";
    }

    @Override
    public String getDescription() {
        return "Afficher le killstreak des joueurs";
    }

    @Override
    public String getSyntax() {
        return "/score viewspree";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==1){
            if(!Util.spreeList.isEmpty())
            {
                for(Map.Entry<String, Integer> e : Util.spreeList.entrySet())
                {
                    String p = e.getKey();
                    int spree = e.getValue();

                    msg(player, "&9» &a"+p+" &6est en série de &b"+spree);
                }
            }
        }else
            msg(player, getSyntax());


    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==1){
            if(!Util.spreeList.isEmpty())
            {
                for(Map.Entry<String, Integer> e : Util.spreeList.entrySet())
                {
                    String p = e.getKey();
                    int spree = e.getValue();

                    log( "&9» &a"+p+" &6est en série de &b"+spree);
                }
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
