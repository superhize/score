package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class IgnoreCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "ignore";
    }

    @Override
    public String getDescription() {
        return "Supprimer un joueur de l'event";
    }

    @Override
    public String getSyntax() {
        return "/score ignore <player>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==2){
            String second = args[1];
            Player pa = Bukkit.getPlayer(second);
            if(pa.isOnline() && pa!=null)
            {
                list.remove(pa);
                msg(player, "&9» &aLe joueur "+pa.getName()+" n'est plus pris en compte pour l'event.");
            }
            else
            {
                OfflinePlayer off = Bukkit.getOfflinePlayer(second);
                list.remove(off.getName());
                msg(player, "&9» &aLe joueur "+off.getName()+" n'est plus pris en compte pour l'event.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length == 2){
            String second = args[1];
            Player pa = Bukkit.getPlayer(second);
            if(pa.isOnline() && pa!=null)
            {
                list.remove(pa);
                log( "&9» &aLe joueur "+pa.getName()+" n'est plus pris en compte pour l'event.");
            }
            else
            {
                OfflinePlayer off = Bukkit.getOfflinePlayer(second);
                list.remove(off.getName());
                log( "&9» &aLe joueur "+off.getName()+" n'est plus pris en compte pour l'event.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
