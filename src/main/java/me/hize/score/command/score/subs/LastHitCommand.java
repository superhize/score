package me.hize.score.command.score.subs;

import me.hize.score.Util;
import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

import static me.hize.score.Score.*;
import static me.hize.score.ScoreUtil.*;

public class LastHitCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "viewlasthit";
    }

    @Override
    public String getDescription() {
        return "Afficher qui a frappé qui";
    }

    @Override
    public String getSyntax() {
        return "/score viewlasthit";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==1){
            if(!Util.lastHit.isEmpty())
            {
                msg(player, "");
                for(Map.Entry<String, String> e : Util.lastHit.entrySet())
                {
                    String v = e.getKey();
                    String a = e.getValue();
                    msg(player, "&9» &a"+v+" &6a été frappé pour la dernière fois par &a"+a);
                }
                msg(player, "");
            }
            else
            {
                msg(player, "&9» &aPersonne n'a encore frappé personne.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==1){
            if(!Util.lastHit.isEmpty())
            {
                log( "");
                for(Map.Entry<String, String> e : Util.lastHit.entrySet())
                {
                    String v = e.getKey();
                    String a = e.getValue();
                    log( "&9» &a"+v+" &6a été frappé pour la dernière fois par &a"+a);
                }
                log( "");
            }
            else
            {
                logWarn( "&9» &aPersonne n'a encore frappé personne.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
