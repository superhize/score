package me.hize.score.command.score.subs;

import me.hize.score.Util;
import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class ViewKillCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "viewkill";
    }

    @Override
    public String getDescription() {
        return "Afficher les kills des joueurs";
    }

    @Override
    public String getSyntax() {
        return "/score viewkill";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            if (!Util.kills.isEmpty()) {
                for (Map.Entry<String, Integer> e : Util.kills.entrySet()) {
                    String p = e.getKey();
                    int kill = e.getValue();

                    msg(player, "&9» &a" + p + " &6a &a" + kill + " &6kill(s)");
                }
            } else {
                msg(player, "&9» &aPersonne n'a encore de kills.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 1) {
            if (!Util.kills.isEmpty()) {
                for (Map.Entry<String, Integer> e : Util.kills.entrySet()) {
                    String p = e.getKey();
                    int kill = e.getValue();

                    log("» " + p + " a " + kill + " kill(s)");
                }
            } else {
                log("» Personne n'a encore de kills.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
