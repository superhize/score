package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class RemoveCmdCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "removecmd";
    }

    @Override
    public String getDescription() {
        return "Supprimer une commande éxécutée";
    }

    @Override
    public String getSyntax() {
        return "/score removecmd <onlose/ondeath> <section> <index>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==4){
            String second = args[1];
            String third = args[2];
            String four = args[3];
            refreshConfig();
            if (config.isConfigurationSection(third)) {
                if (second.equals("onlose")) {
                    int test = 0;
                    try {
                        test = Integer.parseInt(four);
                    } catch (NumberFormatException e) {
                        msg(player, "&c» " + four + " n'est pas un chiffre valide. /score viewcmd <section>");
                        return;
                    }
                    ConfigurationSection sec = config.getConfigurationSection(third);
                    List<String> oldcmd = sec.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                    String cmd = oldcmd.get(test);
                    oldcmd.remove(test);
                    sec.set(EVENT_COMMAND_ON_REMOVE_LOSE, oldcmd);
                    refreshConfig();
                    msg(player, "&9» Commande &b[&d" + test + "&b] &9=> &b(&d" + cmd + "&b) &9retiré.");
                } else if (second.equals("ondeath")) {
                    int test = 0;
                    try {
                        test = Integer.parseInt(four);
                    } catch (NumberFormatException e) {
                        msg(player, "&c» " + four + " n'est pas un chiffre valide. /score viewcmd <section>");
                        return;
                    }
                    ConfigurationSection sec = config.getConfigurationSection(third);
                    List<String> oldcmd = sec.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);
                    String cmd = oldcmd.get(test);
                    oldcmd.remove(test);
                    sec.set(EVENT_COMMAND_ON_REMOVE_DEATH, oldcmd);
                    refreshConfig();
                    msg(player, "&9» Commande &b[&d" + test + "&b] &9=> &b(&d" + cmd + "&b) &9retiré.");
                } else
                    msg(player, getSyntax());
            } else {
                msg(player, "&9» &cLa configuration «" + third + "» n'exsite pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==4){
            String second = args[1];
            String third = args[2];
            String four = args[3];
            refreshConfig();
            if (config.isConfigurationSection(third)) {
                if (second.equals("onlose")) {
                    int test = 0;
                    try {
                        test = Integer.parseInt(four);
                    } catch (NumberFormatException e) {
                        logError("» " + four + " n'est pas un chiffre valide. /score viewcmd <section>");
                        return;
                    }
                    ConfigurationSection sec = config.getConfigurationSection(third);
                    List<String> oldcmd = sec.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                    String cmd = oldcmd.get(test);
                    oldcmd.remove(test);
                    sec.set(EVENT_COMMAND_ON_REMOVE_LOSE, oldcmd);
                    refreshConfig();
                    log("» Commande [" + test + "] => (" + cmd + ") retiré.");
                } else if (second.equals("ondeath")) {
                    int test = 0;
                    try {
                        test = Integer.parseInt(four);
                    } catch (NumberFormatException e) {
                        logError("» " + four + " n'est pas un chiffre valide. /score viewcmd <section>");
                        return;
                    }
                    ConfigurationSection sec = config.getConfigurationSection(third);
                    List<String> oldcmd = sec.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);
                    String cmd = oldcmd.get(test);
                    oldcmd.remove(test);
                    sec.set(EVENT_COMMAND_ON_REMOVE_DEATH, oldcmd);
                    refreshConfig();
                    log("» Commande [" + test + "] => (" + cmd + ") retiré.");
                } else
                    log(getSyntax());
            } else {
                logError("» La configuration " + third + " n'exsite pas.");
            }
        }

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if (args.length == 2) {
            arg.add("onlose");
            arg.add("ondeath");
            return arg;
        } else if (args.length == 3) {
            return getSection();
        } else if (args.length == 4) {
            ConfigurationSection sec = config.getConfigurationSection(args[2]);
            List<String> cmds = new LinkedList<>();

            if (args[1].equalsIgnoreCase("onlose"))
                cmds = sec.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
            else if (args[1].equalsIgnoreCase("ondeath"))
                cmds = sec.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);

            for (int i = 0; i < cmds.size(); i++)
                arg.add(String.valueOf(i));
            return arg;
        }

        return null;
    }
}
