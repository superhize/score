package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class PlayerCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "player";
    }

    @Override
    public String getDescription() {
        return "Interagir avec le joueur";
    }

    @Override
    public String getSyntax() {
        return "/score player <player> <set/add/[remove]> <[section]> <number>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 4) {
            String second = args[1];
            String third = args[2];
            String four = args[3];
            String playername = second;

            Player p = Bukkit.getPlayer(playername);
            if (p != null) {
                if (third.equals("set")) {
                    HandleSet(p, four);
                } else if (third.equals("add")) {
                    refreshConfig();
                    int num = 0;
                    try {
                        num = Integer.parseInt(four);
                    } catch (NumberFormatException e) {
                        msg(player, "§9» §a" + four + " &cn'est pas un nombre correct.");
                    }

                    if (list.containsKey(p)) {
                        int before = list.get(p);
                        list.put(p, (before + num));
                        msg(player, "&9» Le joueur &a" + p.getName() + " &9a maintenant §a" + (before + num) + " §9vie(s)");
                    }
                } else {
                    msg(player, getSyntax());
                }
            } else {
                msg(player, "&9» &aLe joueur §c" + playername + " &9n'existe pas/n'est pas en ligne.");
            }
        } else if (args.length == 5) {
            // /score player HiZe_ remove knockem 1
            //          0      1     2      3     4
            String p = args[1];
            String remove = args[2];
            String section = args[3];
            String life = args[4];
            if (remove.equals("remove")) {

                HandleRemove(p, remove, section, life);

            }
        }

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {

        if (args.length == 4) {
            String second = args[1];
            String third = args[2];
            String four = args[3];
            String playername = second;

            Player p = Bukkit.getPlayer(playername);
            if (p != null) {
                if (third.equals("set")) {
                    HandleSet(p, four);
                } else if (third.equals("add")) {
                    refreshConfig();
                    int num = 0;
                    try {
                        num = Integer.parseInt(four);
                    } catch (NumberFormatException e) {
                        logError("» " + four + " n'est pas un nombre correct.");
                        return;
                    }

                    if (list.containsKey(p)) {
                        int before = list.get(p);
                        list.put(p, (before + num));
                        log("» Le joueur " + p.getName() + " a maintenant " + (before + num) + "vie(s)");

                    }
                } else {
                    logError(getSyntax());
                }
            } else {
                logError("» Le joueur " + playername + " n'existe pas/n'est pas en ligne.");
            }
        } else if (args.length == 5) {
            // /score player HiZe_ remove knockem 1
            //          0      1     2      3     4
            String p = args[1];
            String remove = args[2];
            String section = args[3];
            String life = args[4];
            if (remove.equals("remove")) {

                HandleRemove(p, remove, section, life);

            }
        }
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if (args.length == 3) {
            arg.add("set");
            arg.add("add");
            arg.add("remove");
            return arg;
        } else if (args.length == 4) {
            return getSection();
        }

        return null;
    }
}
