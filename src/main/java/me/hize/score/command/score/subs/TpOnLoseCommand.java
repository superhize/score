package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class TpOnLoseCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "teleport-on-lose";
    }

    @Override
    public String getDescription() {
        return "Définir le point de téléportation à la perte d'une vie";
    }

    @Override
    public String getSyntax() {
        return "/score teleport-on-lose <section> add";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==3){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            String section = second;
            if(config.isConfigurationSection(section))
            {
                if(third.equals("add"))
                {
                    ConfigurationSection sec = config.getConfigurationSection(section);
                    String coord = player.getLocation().getX()+","+player.getLocation().getY()+","+player.getLocation().getZ();

                    List<String> oldcmd = sec.getStringList("teleport-on-lose");
                    oldcmd.add(coord);

                    sec.set("teleport-on-lose", oldcmd);
                    refreshConfig();
                    msg(player, "§9» &aSpawn ajoutée.");
                }

            }
            else
            {
                msg(player, "&9» &cLa configuration «"+section+"» n'exsite pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==3){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            String section = second;
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                sec.set("teleport-on-lose", third);
                refreshConfig();
                log("» Valeur de teleport-on-lose changé en "+third+" pour "+section);
            }
            else
            {
                logError("» La configuration "+section+" n'existe pas.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();


        if (args.length == 2) {
            return getSection();
        }else if (args.length == 3){
            arg.add("add");
            return arg;
        }


        return null;
    }
}
