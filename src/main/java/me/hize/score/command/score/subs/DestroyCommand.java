package me.hize.score.command.score.subs;

import me.hize.score.Util;
import me.hize.score.command.score.ScoreSubCommand;
import me.tigerhix.lib.scoreboard.type.Scoreboard;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class DestroyCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "destroy";
    }

    @Override
    public String getDescription() {
        return "&5Réinitialiser l'event";
    }

    @Override
    public String getSyntax() {
        return "/score destroy";
    }

    @Override
    public void perform(Player player, String[] args) {

        if (args.length == 1) {
            Iterator<Scoreboard> it = sclist.iterator();
            while (it.hasNext()) {
                Scoreboard scb = it.next();
                scb.deactivate();
                scb = null;
            }

            list.clear();
            sclist.clear();
            scoreboard = null;
            Util.kills.clear();
            Util.winners.clear();
            Util.CURRENT_EVENT_NAME = "";
            participant.clear();
            log("» Event réinitialiser.");
        }else
            msg(player, getSyntax());
    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 1) {
            Iterator<Scoreboard> it = sclist.iterator();
            while (it.hasNext()) {
                Scoreboard scb = it.next();
                scb.deactivate();
                scb = null;
            }

            list.clear();
            sclist.clear();
            scoreboard = null;
            Util.kills.clear();
            Util.winners.clear();
            Util.CURRENT_EVENT_NAME = "";
            participant.clear();
            log("» Event réinitialiser.");
        }else
            logError(getSyntax());
    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
