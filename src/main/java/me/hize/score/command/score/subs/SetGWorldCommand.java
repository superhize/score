package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class SetGWorldCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "setgworld";
    }

    @Override
    public String getDescription() {
        return "Définir le monde global";
    }

    @Override
    public String getSyntax() {
        return "/score setgworld <nom>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==2){
            String second = args[1];
            config.set("globalworld", second);
            refreshConfig();
            msg(player, "&9» &aLe monde global a été défini en &6"+second);
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==2){
            String second = args[1];
            config.set("globalworld", second);
            refreshConfig();
            log( "&9» &aLe monde global a été défini en &6"+second);
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            for (int i = 0; i < Bukkit.getServer().getWorlds().size(); i++){
                arg.add(Bukkit.getWorlds().get(i).getName());
            }
            return arg;
        }

        return null;
    }
}
