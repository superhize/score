package me.hize.score.command.score;

import me.hize.score.Util;
import me.hize.score.command.score.subs.*;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.ScoreUtil.*;
import static me.hize.score.command.score.subs.HelpCommand.displayScoreHelp;

public class ScoreCommandManager implements TabExecutor {


    private static ArrayList<ScoreSubCommand> subCommands = new ArrayList<>();

    public ScoreCommandManager() {

        subCommands.add(new CreateboardCommand());
        subCommands.add(new CreateSectionCommand());
        subCommands.add(new DestroyCommand());
        subCommands.add(new HelpCommand());
        subCommands.add(new IgnoreCommand());
        subCommands.add(new LastHitCommand());
        subCommands.add(new RemoveSectionCommand());
        subCommands.add(new SetGWorldCommand());
        subCommands.add(new SetNameCommand());
        subCommands.add(new SetRegionCommand());
        subCommands.add(new SetTitleCommand());
        subCommands.add(new SetWorldCommand());
        subCommands.add(new SpreeCommand());
        subCommands.add(new ToggleCommand());
        subCommands.add(new TpOnDeathCommand());
        subCommands.add(new TpOnLoseCommand());
        subCommands.add(new ViewCmdCommand());
        subCommands.add(new ViewKillCommand());
        subCommands.add(new ViewPlayerCommand());
        subCommands.add(new ViewSectionCommand());
        subCommands.add(new PlayerCommand());
        subCommands.add(new AddCmdCommand());
        subCommands.add(new RemoveCmdCommand());
        subCommands.add(new TutoCommand());
        subCommands.add(new InfoCommand());

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = null;

        if (sender instanceof Player)
            p = (Player) sender;

        if ((p != null && p.hasPermission(Util.PERM)) || !(sender instanceof Player)) {
            if (args.length > 0) {
                for (int i = 0; i < getScoreSubCommands().size(); i++) {
                    if (args[0].equalsIgnoreCase(getScoreSubCommands().get(i).getName())) {
                        if (sender instanceof Player)
                            getScoreSubCommands().get(i).perform(p, args);
                        else
                            getScoreSubCommands().get(i).consolePerform(Bukkit.getServer().getConsoleSender(), args);
                    }
                }
            } else if (args.length == 0) {
                if (sender instanceof Player)
                    displayScoreHelp(p, 1);
                else
                    log("------------------------------------");
                for (int i = 0; i < getScoreSubCommands().size(); i++) {
                    if (!(sender instanceof Player))
                        log(getScoreSubCommands().get(i).getSyntax() + " - " + getScoreSubCommands().get(i).getDescription());
                }
                if (!(sender instanceof Player))
                    log("------------------------------------");
            }
        } else {
            if (p != null) {
                msg(p, "  &9»  &6Plugin d'event crée par &cHiZe_");
            }
        }

        return true;
    }

    public static ArrayList<ScoreSubCommand> getScoreSubCommands() {
        return subCommands;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {

        if (args.length == 1) {
            ArrayList<String> subCommandArg = new ArrayList<>();

            for (int i = 0; i < getScoreSubCommands().size(); i++) {
                subCommandArg.add(getScoreSubCommands().get(i).getName());
            }
            return subCommandArg;
        } else if(args.length >= 2){
            for (int i = 0; i < getScoreSubCommands().size(); i++) {
                if (args[0].equalsIgnoreCase(getScoreSubCommands().get(i).getName())) {
                        return getScoreSubCommands().get(i).getSubcommandArgs((Player)sender, args);
                }
            }
        }

        return null;
    }
}
