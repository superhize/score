package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class SetRegionCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "setregion";
    }

    @Override
    public String getDescription() {
        return "Définir la region de l'event";
    }

    @Override
    public String getSyntax() {
        return "/score setregion <nom> <section>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==3){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            String section = second;
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                sec.set("region", third);
                refreshConfig();
                msg(player, "§9» La region a été region changé en &6"+third+" &apour &6"+second);
            }
            else
            {
                msg(player, "&9» &cLa configuration «"+section+"» n'exsite pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==3){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            String section = second;
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                sec.set("region", third);
                refreshConfig();
                System.out.println("» Valeur de region changé en "+third+" pour "+second);
            }
            else
            {
                logError("» La configuration "+section+" n'existe pas.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            return getRegion(player);
        }else if(args.length == 3){
            return getSection();
        }

        return null;
    }
}
