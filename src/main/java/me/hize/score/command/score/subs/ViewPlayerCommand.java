package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class ViewPlayerCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "viewplayer";
    }

    @Override
    public String getDescription() {
        return "View player list";
    }

    @Override
    public String getSyntax() {
        return "/score viewplayer";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length == 1) {
            if (!participant.isEmpty()) {
                msg(player, "");
                for (String name : participant) {

                    msg(player, "&9» &a" + name);

                }
                msg(player, "");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if (args.length == 1) {
            if (!participant.isEmpty()) {
                log("");
                for (String name : participant) {

                    log("&9» &a" + name);

                }
                log("");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {
        return null;
    }
}
