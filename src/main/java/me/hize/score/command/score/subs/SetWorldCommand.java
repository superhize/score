package me.hize.score.command.score.subs;

import me.hize.score.command.score.ScoreSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

import static me.hize.score.Score.logError;
import static me.hize.score.ScoreUtil.*;

public class SetWorldCommand extends ScoreSubCommand {
    @Override
    public String getName() {
        return "setworld";
    }

    @Override
    public String getDescription() {
        return "Définir le monde de l'event";
    }

    @Override
    public String getSyntax() {
        return "/score setworld <nom> <section>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if(args.length==3){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            String section = second;
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                sec.set("world", third);
                refreshConfig();
                msg(player, "§9» Le monde a été changé en §6"+third+" &apour &6"+second);
            }
            else
            {
                msg(player, "&9» &cLa configuration «"+section+"» n'exsite pas.");
            }
        }else
            msg(player, getSyntax());

    }

    @Override
    public void consolePerform(ConsoleCommandSender console, String[] args) {
        if(args.length==3){
            String second = args[1];
            String third = args[2];
            refreshConfig();
            String section = second;
            if(config.isConfigurationSection(section))
            {
                ConfigurationSection sec = config.getConfigurationSection(section);
                sec.set("world", third);
                refreshConfig();
                System.out.println("» Valeur de monde changé en "+third+" pour "+second);
            }
            else
            {
                logError("» La configuration "+section+" n'existe pas.");
            }
        }else
            logError(getSyntax());

    }

    @Override
    public List<String> getSubcommandArgs(Player player, String[] args) {

        List<String> arg = new LinkedList<>();

        if(args.length == 2){
            for (int i = 0; i < Bukkit.getServer().getWorlds().size(); i++){
                arg.add(Bukkit.getWorlds().get(i).getName());
            }
            return arg;
        } else if (args.length == 3) {
            return getSection();
        }

        return null;
    }
}
