package me.hize.score;

import me.hize.score.configuration.BuildBattleConfiguration;
import org.bukkit.configuration.Configuration;

import java.util.*;
import java.util.stream.Collectors;

public class BuildBattleUtil {


    public static Map<String, Boolean> hasvoted1 = new HashMap<>();

    public static Map<String, Boolean> hasvoted2 = new HashMap<>();

    public static Map<String, Boolean> hasvoted3 = new HashMap<>();

    public static Map<String, Boolean> hasvoted4 = new HashMap<>();

    public static Map<String, Integer> vote = new HashMap<>();

    public static Map<String, String> ltheme = new HashMap<>();

    public static BuildBattleConfiguration config = Score.getInstance().configurations.getBuildBattle();

    public static Map<String, Integer> sortByValue(Map<String, Integer> map) {
        Map<String, Integer> sorted = (Map<String, Integer>) map.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, java.util.LinkedHashMap::new));
        return sorted;
    }

    public static List<String> getMostVote(Map<String, Integer> map) {
        List<String> resultList = new ArrayList<>();
        int currentMaxValuevalue = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (((Integer) entry.getValue()).intValue() > currentMaxValuevalue) {
                resultList.clear();
                resultList.add(entry.getKey());
                currentMaxValuevalue = ((Integer) entry.getValue()).intValue();
                continue;
            }
            if (((Integer) entry.getValue()).intValue() == currentMaxValuevalue)
                resultList.add(entry.getKey());
        }
        return resultList;
    }

    public static String getNoPerm() {
        return " &9»  &cVous n'avez pas la permission.";
    }

    public static List<String> getSection() {
        List<String> section = new LinkedList<>();

        Map<String, Object> map = config.getConfig().getValues(false);
        for (Map.Entry<String, Object> e : map.entrySet()) {
            if (e.getKey().equalsIgnoreCase("players")) break;
            section.add(e.getKey());
        }

        return section;
    }

}
