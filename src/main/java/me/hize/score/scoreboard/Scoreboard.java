package me.hize.score.scoreboard;

import me.hize.score.Score;
import me.hize.score.ScoreUtil;
import me.tigerhix.lib.scoreboard.common.EntryBuilder;
import me.tigerhix.lib.scoreboard.common.Strings;
import me.tigerhix.lib.scoreboard.common.animate.HighlightedString;
import me.tigerhix.lib.scoreboard.type.Entry;
import me.tigerhix.lib.scoreboard.type.ScoreboardHandler;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

public class Scoreboard implements ScoreboardHandler {

    private String event;

    public Scoreboard(String e)
    {
        event=e;
    }

    private final HighlightedString highlighted = new HighlightedString("Vies", "&6", "&e");


    @Override
    public String getTitle(Player player)
    {
        return Strings.format(Score.getInstance().getConfig().getString(event+".title"));
    }

    @Override
    public List<Entry> getEntries(Player player) {

        EntryBuilder b = new EntryBuilder();

        b.next("    " + Strings.format("&a"+ Score.getInstance().getConfig().getString(event+".name")));
        b.next("    " + highlighted.next());
        b.blank();

        for(Map.Entry<Player, Integer> e : sortByValue(ScoreUtil.list).entrySet())
        {
            int n = sortByValue(ScoreUtil.list).get(e.getKey());
            String color = n==3?"&2":n==2?"&a":n==1?"&e":"&c";
            b.next(((sortByValue(ScoreUtil.list).get(e.getKey()) >= 0) ? "    &b&l" : "    &c&l") + e.getKey().getName() + " » " + ((sortByValue(ScoreUtil.list).get(e.getKey()) >= 0) ? color+sortByValue(ScoreUtil.list).get(e.getKey()) : "éliminé !"));
        }

        b.blank();

        return b.build();
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return  map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (x,y)-> {throw new AssertionError();},
                        LinkedHashMap::new
                ));
    }



}