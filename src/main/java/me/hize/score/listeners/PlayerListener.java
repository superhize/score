package me.hize.score.listeners;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import me.hize.score.Score;
import me.hize.score.Util;
import org.bukkit.Bukkit;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import static me.hize.score.ScoreUtil.*;

public class PlayerListener implements Listener {

    @EventHandler
    public void PlayerDamagePlayer(EntityDamageByEntityEvent e)
    {
        if(Util.CURRENT_EVENT_NAME.isEmpty())
            return;
        if(e.getEntity().getWorld().getName().equals(Score.getInstance().getConfig().get("globalworld")))
        {

            String event = "";
            if(Util.CURRENT_EVENT_NAME.equals("knockem"))
                event = "knockem";
            else if(Util.CURRENT_EVENT_NAME.equals("arrow"))
                event = "arrow";

            if(Score.getInstance().getConfig().isConfigurationSection(event))
            {
                if(e.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE) || e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK))
                {
                    if(e.getEntity() instanceof Player && e.getDamager() instanceof Player || e.getDamager() instanceof Arrow)
                    {
                        Player victime = (Player)e.getEntity();
                        Player assassin=null;

                        if(e.getDamager() instanceof Arrow){
                            Arrow s = (Arrow) e.getDamager();
                            if (s.getShooter() instanceof Player){
                                assassin = (Player)s.getShooter();
                            }
                        }
                        else
                            assassin = (Player)e.getDamager();


                        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
                        container.get(BukkitAdapter.adapt(victime.getWorld()));


                        for(ProtectedRegion r : container.get(BukkitAdapter.adapt(victime.getWorld())).getApplicableRegions(BukkitAdapter.asBlockVector(victime.getLocation())))
                        {
                            if(Score.getInstance().getConfig().get(event+".region").equals(r.getId()))
                            {
                                Util.lastHit.put(victime.getName(), assassin.getName());

                            }
                        }
                    }
                }
            }

        }
    }


    @EventHandler
    public void Damage(EntityDamageEvent e)
    {
        if(Util.CURRENT_EVENT_NAME.isEmpty())
            return;

        if(e.getEntity().getWorld().getName().equals(Score.getInstance().getConfig().get("globalworld")))
        {

            String event = "";
            if(Util.CURRENT_EVENT_NAME.equals("knockem"))
                event = "knockem";
            else if(Util.CURRENT_EVENT_NAME.equals("arrow"))
                event = "arrow";

            if(Score.getInstance().getConfig().isConfigurationSection(event))
            {
                if(e.getEntity() instanceof Player)
                {
                    Player victime = (Player)e.getEntity();

                    RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
                    for(ProtectedRegion r : container.get(BukkitAdapter.adapt(victime.getWorld())).getApplicableRegions(BukkitAdapter.asBlockVector(victime.getLocation())))
                    {
                        if(Score.getInstance().getConfig().get(event+".region").equals(r.getId()))
                        {
                            if (((victime.getHealth() - e.getFinalDamage()) <= 0))
                            {
                                e.setCancelled(true);
                                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "score "+victime.getName()+" remove "+Util.CURRENT_EVENT_NAME+" 1");
                            }

                        }
                    }
                }
            }
        }

    }

    @EventHandler
    public void OnQuit(PlayerQuitEvent e)
    {
        if(Util.CURRENT_EVENT_NAME.isEmpty())
            return;

        Player player = e.getPlayer();
        if(player.getWorld().getName().equals(Score.getInstance().getConfig().get("globalworld")))
        {
            String event = "";
            if(Util.CURRENT_EVENT_NAME.equals("knockem"))
                event = "knockem";
            else if(Util.CURRENT_EVENT_NAME.equals("arrow"))
                event = "arrow";


            if(Score.getInstance().getConfig().isConfigurationSection(event))
            {
                RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
                ApplicableRegionSet regions = container.get(BukkitAdapter.adapt(player.getWorld())).getApplicableRegions(BukkitAdapter.asBlockVector(player.getLocation()));

                for(ProtectedRegion r : regions.getRegions())
                {
                    if (r.getId().equals(Score.getInstance().getConfig().getString(event+".region")))
                    {
                        if(list.containsKey(player))
                        {
                            list.remove(player);
                            participant.remove(player.getName());
                            //TODO: add to list when player reconnect => teleport to spec
                        }
                    }
                }
            }
        }
    }

    //TODO: reconnect after leaving (knockem)
    public void onJoin(PlayerJoinEvent e){}


}
