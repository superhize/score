package me.hize.score.configuration;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import me.hize.score.Score;
import me.hize.score.command.buildbattle.BuildBattleCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class BuildBattleConfiguration {
    private Score pl;

    private File buildbattleFile;

    private FileConfiguration buildbattle;

    public BuildBattleConfiguration(Score main) {
        this.pl = main;
        createConfig();
    }

    private void createConfig() {
        this.buildbattleFile = new File(this.pl.getDataFolder(), "buildbattle.yml");
        this.buildbattle = (FileConfiguration)new YamlConfiguration();
        try {
            if (!this.buildbattleFile.exists())
                this.buildbattleFile.createNewFile();
        } catch (UnsupportedEncodingException e) {
            Bukkit.getConsoleSender().sendMessage("is an error with the configuration buildbattle.yml. You should perform a reload.");
            e.printStackTrace();
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage("is an error with the configuration buildbattle.yml. You should perform a reload.");
            e.printStackTrace();
        }
        try {
            this.buildbattle.load(this.buildbattleFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        saveConfig();
    }

    public void saveConfig() {
        try {
            this.buildbattle.save(this.buildbattleFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getConfig() {
        return this.buildbattle;
    }

    public void link(String player, String casename, CommandSender s) {
        if (!this.buildbattle.isConfigurationSection(casename) &&
                s instanceof Player) {
            ((Player)s).sendMessage("&9&cNom introuvable.");
            return;
        }
        this.buildbattle.getConfigurationSection(casename).set("owner", player);
        if (this.buildbattle.isConfigurationSection("players")) {
            this.buildbattle.getConfigurationSection("players").set(player + ".genial", Integer.valueOf(0));
            this.buildbattle.getConfigurationSection("players").set(player + ".bien", Integer.valueOf(0));
            this.buildbattle.getConfigurationSection("players").set(player + ".bof", Integer.valueOf(0));
            this.buildbattle.getConfigurationSection("players").set(player + ".nul", Integer.valueOf(0));
        } else {
            this.buildbattle.createSection("players");
            this.buildbattle.getConfigurationSection("players").set(player + ".genial", Integer.valueOf(0));
            this.buildbattle.getConfigurationSection("players").set(player + ".bien", Integer.valueOf(0));
            this.buildbattle.getConfigurationSection("players").set(player + ".bof", Integer.valueOf(0));
            this.buildbattle.getConfigurationSection("players").set(player + ".nul", Integer.valueOf(0));
        }
        saveConfig();
    }

    public void add(String casename, int pts, String player, CommandSender s) {
        if (!this.buildbattle.isConfigurationSection(casename)) {
            if (s instanceof Player) {
                ((Player)s).sendMessage("&9&cNom introuvable.");
                return;
            }
            return;
        }
        int oldpts = this.buildbattle.getConfigurationSection(casename).getInt("points");
        this.buildbattle.getConfigurationSection(casename).set("points", Integer.valueOf(oldpts + pts));
        String pal = "";
        if (pts == 0) {
            pal = "nul";
        } else if (pts == 1) {
            pal = "bof";
        } else if (pts == 2) {
            pal = "bien";
        } else if (pts == 3) {
            pal = "genial";
        }
        List<String> vote = this.buildbattle.getConfigurationSection(casename).getStringList("vote." + pal);
        vote.add(player);
        this.buildbattle.getConfigurationSection(casename).set("vote." + pal, vote);
        int votebefore = 0;
        try {
            votebefore = this.buildbattle.getConfigurationSection("players").getInt(player + "." + pal);
        } catch (NullPointerException e) {
            votebefore = 0;
        } catch (NumberFormatException e) {
            votebefore = 0;
        }
        if (s instanceof Player) {
            String v = (pts == 3) ? "&2Génial": ((pts == 2) ? "&aBien" : ((pts == 1) ? "&cBof" : "&4Nul"));
            ((Player)s).sendMessage(ChatColor.translateAlternateColorCodes('&', "&9&aVous avez voté " + v + " &apour la construction de &6" + this.buildbattle.getConfigurationSection(casename).getString("owner")));
        }
        this.buildbattle.getConfigurationSection("players").set(player + "." + pal, Integer.valueOf(votebefore + 1));
        saveConfig();
    }

    public void remove(String casename, int pts) {
        if (!this.buildbattle.isConfigurationSection(casename))
            this.buildbattle.createSection(casename);
        int oldpts = this.buildbattle.getConfigurationSection(casename).getInt("points");
        this.buildbattle.getConfigurationSection(casename).set("points", Integer.valueOf(oldpts - pts));
        saveConfig();
    }

    public void reset() {
        Map<String, Object> map = this.buildbattle.getValues(false);
        for (Map.Entry<String, Object> e : map.entrySet()) {
            String section = (String)e.getKey();
            if (!section.equalsIgnoreCase("players")) {
                this.buildbattle.getConfigurationSection(section).set("owner", new String());
                this.buildbattle.getConfigurationSection(section).set("points", new String());
                this.buildbattle.getConfigurationSection(section).set("vote.genial", new LinkedList());
                this.buildbattle.getConfigurationSection(section).set("vote.bien", new LinkedList());
                this.buildbattle.getConfigurationSection(section).set("vote.bof", new LinkedList());
                this.buildbattle.getConfigurationSection(section).set("vote.nul", new LinkedList());
                saveConfig();
                continue;
            }
            this.buildbattle.set("players", null);
            saveConfig();
        }
        BuildBattleCommand.hasvoted1.clear();
        BuildBattleCommand.hasvoted2.clear();
        BuildBattleCommand.hasvoted3.clear();
        BuildBattleCommand.hasvoted4.clear();
        BuildBattleCommand.ltheme.clear();
        BuildBattleCommand.vote.clear();
    }
}
