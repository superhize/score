package me.hize.score.configuration;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import me.hize.score.Score;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class CheckpointConfiguration {
    private Score pl;

    private File checkpointFile;

    private FileConfiguration checkpoint;

    public CheckpointConfiguration(Score main) {
        this.pl = main;
        createConfig();
    }

    private void createConfig() {
        this.checkpointFile = new File(this.pl.getDataFolder(), "checkpoint.yml");
        this.checkpoint = (FileConfiguration)new YamlConfiguration();
        try {
            if (!this.checkpointFile.exists())
                this.checkpointFile.createNewFile();
        } catch (UnsupportedEncodingException e) {
            Bukkit.getConsoleSender().sendMessage("is an error with the configuration checkpoint.yml. You should perform a reload.");
            e.printStackTrace();
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage("is an error with the configuration checkpoint.yml. You should perform a reload.");
            e.printStackTrace();
        }
        try {
            this.checkpoint.load(this.checkpointFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        saveConfig();
    }

    public void saveConfig() {
        try {
            this.checkpoint.save(this.checkpointFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getCheckPoint() {
        return this.checkpoint;
    }

    public void addCheckpoint(String player, String arg) {
        this.checkpoint.set(player, arg);
        try {
            this.checkpoint.save(this.checkpointFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
