package me.hize.score.configuration;

import me.hize.score.Score;

public class Configurations {
    private CheckpointConfiguration checkpoint;
    private BuildBattleConfiguration buildbattle;

    public Configurations(Score pl) {
        this.checkpoint = new CheckpointConfiguration(pl);
        this.buildbattle = new BuildBattleConfiguration(pl);
    }

    public CheckpointConfiguration getCheckPoints() {
        return this.checkpoint;
    }

    public BuildBattleConfiguration getBuildBattle() {
        return this.buildbattle;
    }
}
