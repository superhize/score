package me.hize.score;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import me.hize.score.command.buildbattle.BuildBattleCommand;
import me.hize.score.command.buildbattle.BuildBattleCommandManager;
import me.hize.score.command.checkpoint.CheckpointCommandManager;
import me.hize.score.command.score.ScoreCommandManager;
import me.hize.score.configuration.Configurations;
import me.hize.score.listeners.PlayerListener;
import me.tigerhix.lib.scoreboard.ScoreboardLib;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class Score extends JavaPlugin {

    public static Score instance;
    public FileConfiguration config;
    public Configurations configurations;

    @Override
    public void onEnable() {
        instance = this;
        config = this.getConfig();
        this.configurations = new Configurations(this);
        ScoreboardLib.setPluginInstance(this);
        getCommand("score").setExecutor(new ScoreCommandManager());
        getCommand("buildbattle").setExecutor(new BuildBattleCommandManager());
        getCommand("checkpoint").setExecutor(new CheckpointCommandManager());
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        config.options().copyDefaults(true);
        saveConfig();
    }

    @Override
    public void onDisable() {
    }

    public static Score getInstance() {
        return instance;
    }

    public WorldGuardPlugin getWorldGuard() {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin) plugin;
    }

    public static void log(String msg) {
        Score.getInstance().getServer().getConsoleSender().sendMessage("[Score] " + msg);
    }

    public static void logWarn(String msg) {
        Score.getInstance().getServer().getConsoleSender().sendMessage("[Score][Warning] " + msg);
    }

    public static void logError(String msg) {
        Score.getInstance().getServer().getConsoleSender().sendMessage("[Score][Erreur] " + msg);
    }

}
