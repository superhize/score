package me.hize.score;

import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Util {

    public static String VERSION = "2.0";
    public static String PERM = "topcraft.score";
    public static String CURRENT_EVENT_NAME = "";
    public static NumberFormat format = new DecimalFormat("#0.00");


    public static Map<String, String> lastHit = new HashMap<>();

    public static Map<String, Integer> kills = new HashMap<>();
    public static Map<Integer, Player> winners = new LinkedHashMap<>();

    public static Map<String, Boolean> isOnSpree = new HashMap<>();

    public static Map<String, Integer> spreeList = new HashMap<>();

    public static String getCheckpoint(String player) {
        if (player.isEmpty())
            return "";
        if ((Score.getInstance()).configurations.getCheckPoints().getCheckPoint().getString(player) == null)
            return "";
        return (Score.getInstance()).configurations.getCheckPoints().getCheckPoint().getString(player);
    }

}
