package me.hize.score;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import me.hize.score.command.score.TutoHelper;
import me.tigerhix.lib.scoreboard.type.Scoreboard;
import org.bukkit.*;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.*;
import java.util.stream.Collectors;

import static me.hize.score.Score.log;
import static me.hize.score.Score.logError;
import static me.hize.score.command.buildbattle.BuildBattleCommandManager.getBuildBattleSubCommands;
import static me.hize.score.command.checkpoint.CheckpointCommandManager.getCheckpointSubCommands;
import static me.hize.score.command.score.ScoreCommandManager.getScoreSubCommands;

public class ScoreUtil {

    public static Configuration config = Score.getInstance().getConfig();
    public static Map<Player, Integer> list = new HashMap<Player, Integer>();
    public static List<String> participant = new LinkedList<>();
    public static Scoreboard scoreboard;
    public static List<Scoreboard> sclist = new LinkedList<Scoreboard>();

    public static String EVENT_COMMAND_ON_REMOVE_LOSE = "command-on-remove.lose";
    public static String EVENT_COMMAND_ON_REMOVE_DEATH = "command-on-remove.death";

    public static final int CMD_PER_PAGE = 5;
    public static final int TUTO_PER_PAGE = 1;
    public static final String LEFT_ARROW = "[◄]";
    public static final String RIGHT_ARROW = "[►]";
    public static final String ARROW_ALLOWED_COLOR = "§2§l";
    public static final String ARROW_DENY_COLOR = "§4§l";
    public static final String colorLeft = ARROW_ALLOWED_COLOR;
    public static final String colorRight = ARROW_ALLOWED_COLOR;

    public static void msg(Player p, String s){
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
    }
    public static void brodcast(String s){
        Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', s));
    }
    public static <K, V extends Comparable<? super V>> Map<K, V> sortBy(Map<K, V> map) {
        return  map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (x,y)-> {throw new AssertionError();},
                        LinkedHashMap::new
                ));
    }
    public static List<String> getMostKill(Map<String, Integer> map){
        final List<String> resultList = new ArrayList<String>();
        int currentMaxValuevalue = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> entry : map.entrySet()){
            if (entry.getValue() > currentMaxValuevalue){
                resultList.clear();
                resultList.add(entry.getKey());
                currentMaxValuevalue = entry.getValue();
            } else if (entry.getValue() == currentMaxValuevalue){
                resultList.add(entry.getKey());
            }
        }
        return resultList;
    }
    public static void spawnFireworks(Location location, int amount){
        Location loc = location;
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(2);
        fwm.addEffect(FireworkEffect.builder().withColor(Color.LIME).flicker(true).trail(true).with(FireworkEffect.Type.BALL_LARGE).with(FireworkEffect.Type.STAR).build());

        fw.setFireworkMeta(fwm);
        fw.detonate();

        for(int i = 0;i<amount; i++){
            Firework fw2 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            fw2.setFireworkMeta(fwm);
        }
    }

    public static void refreshConfig()
    {
        Score.getInstance().saveConfig();
        Score.getInstance().reloadConfig();
        config = Score.getInstance().getConfig();
    }
    public static void OnKillingSpree(String name, int spree) {
        if(spree==3)
        {
            brodcast("");
            brodcast("&b"+name+" &aentre dans une série meurtrière !");
            brodcast("");
            Util.isOnSpree.put(name, true);
        }
        else if(spree==4)
        {
            brodcast("");
            brodcast("&b"+name+" &aest instopable !");
            brodcast("");
            Util.isOnSpree.put(name, true);
        }
        else if(spree==6)
        {
            brodcast("");
            brodcast("&b"+name+" &6est un dieu !");
            brodcast("");
            Util.isOnSpree.put(name, true);
        }
        else if(spree>=8)
        {
            brodcast("");
            brodcast("&b&l"+name+" &5&lest légendaire !");
            brodcast("");
            Util.isOnSpree.put(name, true);
        }


    }
    public static boolean isOnKillingSpree(String name){

        if(Boolean.TRUE.equals(Util.isOnSpree.get(name)))
            return true;

        return false;
    }
    public static void HandleSet(Player p, String third){
        refreshConfig();
        int num=0;
        try
        {
            num = Integer.parseInt(third);
        }
        catch(NumberFormatException e)
        {
            logError("» "+third+" n'est pas un nombre correct.");
            return;
        }

        if(!list.containsKey(p))
        {
            list.put(p, num);
            Util.kills.put(p.getName(), 0);
            Util.lastHit.put(p.getName(), "Pingusman");
            Util.spreeList.put(p.getName(), 0);
            Util.isOnSpree.put(p.getName(), false);
            participant.add(p.getName());
            log("» Le joueur "+p.getName()+" a été ajoutée avec "+num+" vie(s)");
        }
    }
    public static void HandleRemove(String first, String second, String third, String four) {
        //pseudo remove section index
        //   0      1      2      3

        int num=0;
        try
        {
            num = Integer.parseInt(four);
        }
        catch (NumberFormatException e)
        {
            logError("» "+four+" n'est pas un chiffre valide.");
            return;
        }
        Player p = Bukkit.getPlayer(first);
        if(config.isConfigurationSection(third))
        {
            if(p!=null)
            {
                if(list.containsKey(p)){
                    ConfigurationSection sec = config.getConfigurationSection(third);
                    int before = list.get(p);
                    list.put(p, (before-num));
                    int newv = list.get(p);


                    List<String> cmdOnLose = sec.getStringList(EVENT_COMMAND_ON_REMOVE_LOSE);
                    List<String> cmdOnDeath = sec.getStringList(EVENT_COMMAND_ON_REMOVE_DEATH);

                    String killer = "";
                    int kill = 0;
                    int killerkill = 0;
                    int killerspree = 0;

                    if(Util.CURRENT_EVENT_NAME.equals("knockem") || Util.CURRENT_EVENT_NAME.equals("arrow"))
                    {
                        killer = Util.lastHit.get(p.getName());
                        kill = Util.kills.get(p.getName());
                        killerkill = Util.kills.get(killer);
                        killerspree = Util.spreeList.get(killer);
                    }



                    if(newv>=0)
                    {
                        p.sendMessage("§9» Vous avez perdu une vie.");
                        String w = sec.getString("world");
                        List<String> li = sec.getStringList("teleport-on-lose");
                        int random = new Random().nextInt(li.size());
                        String[] c = li.get(random).split(",");
                        double x = Double.parseDouble(c[0]);
                        double y = Double.parseDouble(c[1]);
                        double z = Double.parseDouble(c[2]);

                        Iterator<String> cmdOnLoseIt = cmdOnLose.iterator();
                        while(cmdOnLoseIt.hasNext())
                        {
                            String cmd = cmdOnLoseIt.next();
                            Score.getInstance().getServer().dispatchCommand(Score.getInstance().getServer().getConsoleSender(),
                                    ChatColor.translateAlternateColorCodes('&', cmd.replace("%player%", p.getName())
                                            .replace("%killer%", killer)));
                        }


                        //TODO: Uniquement au knockem
                        if(Util.CURRENT_EVENT_NAME.equals("knockem") || Util.CURRENT_EVENT_NAME.equals("arrow"))
                        {
                                          /*
                                        Update killzzz
                                     */
                            Util.kills.put(killer, (killerkill)+1);
                            Util.spreeList.put(killer, (killerspree)+1);
                                    /*
                                        On spree
                                     */
                            OnKillingSpree(killer, killerspree);

                                    /*
                                        Reset spree
                                     */
                            if(isOnKillingSpree(p.getName()))
                            {
                                brodcast("");
                                brodcast("&9» &a"+killer+" &6a interrompu &a"+p.getName()+" !");
                                brodcast("");
                            }

                            Util.spreeList.put(p.getName(), 0);
                            Util.isOnSpree.put(p.getName(), false);
                        }



                        Location loca = new Location(Bukkit.getWorld(w), x, y, z);
                        p.teleport(loca);
                    }

                    else if(newv<0)
                    {

                        p.sendMessage("§9» Vous avez perdu une vie.");
                        p.sendMessage("§c» Vous n'avez plus de vie, vous êtes éliminé !");
                        String w = sec.getString("world");
                        String[] c = sec.getString("teleport-on-death").split(",");

                        double x = Double.parseDouble(c[0]);
                        double y = Double.parseDouble(c[1]);
                        double z = Double.parseDouble(c[2]);


                        Iterator<String> cmdOnDeathIt = cmdOnDeath.iterator();
                        while(cmdOnDeathIt.hasNext())
                        {
                            String cmd = cmdOnDeathIt.next();
                            Score.getInstance().getServer().dispatchCommand(Score.getInstance().getServer().getConsoleSender(),
                                    ChatColor.translateAlternateColorCodes('&', cmd.replace("%player%", p.getName())
                                            .replace("%killer%", killer)));
                        }


                        //TODO: Uniquement au knockem
                        if(Util.CURRENT_EVENT_NAME.equals("knockem") || Util.CURRENT_EVENT_NAME.equals("arrow"))
                        {
                                              /*
                                        Update killzzz
                                     */
                            Util.kills.put(killer, (killerkill)+1);
                            Util.spreeList.put(killer, (Util.spreeList.get(killer))+1);

                                    /*
                                        On spree
                                     */
                            OnKillingSpree(killer, Util.spreeList.get(killer));

                                    /*
                                        Reset spree
                                     */
                            if(isOnKillingSpree(p.getName()))
                            {
                                brodcast("");
                                brodcast("&9» &a"+killer+" &6a interompu &a"+p.getName()+" !");
                                brodcast("");
                            }

                            Util.spreeList.put(p.getName(), 0);
                            Util.isOnSpree.put(p.getName(), false);
                        }


                        Location loca = new Location(Bukkit.getWorld(w), x, y, z);

                        /*
                        TODO: Annoncer le 1er,2ème,3ème automatiquement.
                        */

                        if(Util.CURRENT_EVENT_NAME.equals("knockem") || Util.CURRENT_EVENT_NAME.equals("arrow"))
                            announceKnockemTop(p, killer);
                        else
                            announceTop(p);




                        if(Util.CURRENT_EVENT_NAME.equals("knockem") || Util.CURRENT_EVENT_NAME.equals("arrow"))
                            announceKnockemWinner(loca);
                        else
                            announceWinner(loca);

                        if(list.containsKey(p))
                            list.remove(p);

                        participant.remove(p.getName());
                        p.teleport(loca);


                    }
                }
            }
            else
            {
                logError("» Le joueur "+second+" n'existe pas/n'est pas en ligne.");
            }
        }
        else
        {
            logError("» La configuration "+third+" n'exsite pas.");
        }
    }


    public static void announceKnockemWinner(Location loca) {
        if(list.size()<=1 && !Util.kills.isEmpty() && !Util.winners.isEmpty())
        {



            brodcast("");
            brodcast("&e======================================");
            brodcast("&9»     &6Event terminé.");
            brodcast("");
            String mostkill = String.join(", ", getMostKill(Util.kills));
            brodcast("&9»     &6Le plus de kills: &b"+mostkill+ " ("+Collections.max(Util.kills.values())+")");
            brodcast("");

            brodcast("&9»     Gagnants: ");
            brodcast("&9»     &a&l"+((Util.winners.get(1)!=null)?Util.winners.get(1).getName()+" (1er) ":"&7&oPersonne. (1er)"));
            brodcast("&9»     &3"+((Util.winners.get(2)!=null)?Util.winners.get(2).getName()+" (2ème) ":"&7&oPersonne. (2ème)"));
            brodcast("&9»     &6"+((Util.winners.get(3)!=null)?Util.winners.get(3).getName()+" (3ème) ":"&7&oPersonne. (3ème)"));
            brodcast("&e======================================");
            brodcast("");

            if(Util.winners.get(1)!=null)
            {
                Player winner = Util.winners.get(1);
                list.remove(winner);
                winner.teleport(loca);
                for(int i=0 ;i<10; i++)
                    spawnFireworks(winner.getLocation(), i);

            }
        }
    }
    public static void announceWinner(Location loca) {
        if(participant.size()<=2)
        {
            brodcast("");
            brodcast("&e======================================");
            brodcast("&9»     &6Event terminé.");
            brodcast("");
            brodcast("&9»     Gagnants: ");
            brodcast("&9»     &a&l"+((Util.winners.get(1)!=null)?Util.winners.get(1).getName()+" (1er) ":"&7&oPersonne. (1er)"));
            brodcast("&9»     &3"+((Util.winners.get(2)!=null)?Util.winners.get(2).getName()+" (2ème) ":"&7&oPersonne. (2ème)"));
            brodcast("&9»     &6"+((Util.winners.get(3)!=null)?Util.winners.get(3).getName()+" (3ème) ":"&7&oPersonne. (3ème)"));
            brodcast("&e======================================");
            brodcast("");

            if(Util.winners.get(1)!=null)
            {
                Player winner = Util.winners.get(1);
                list.remove(winner);
                winner.teleport(loca);
                for(int i=0 ;i<10; i++)
                    spawnFireworks(winner.getLocation(), i);

            }
        }
    }
    public static void announceKnockemTop(Player p, String killer){
        int size = list.size();
        if(size<=3)
        {
            brodcast("");
            switch(size)
            {
                case 3:
                    brodcast("&9»          &a"+p.getName()+" &6termine 3ème de l'event "+Util.CURRENT_EVENT_NAME);
                    Util.winners.put(3, p);
                    break;
                case 2:
                    brodcast("&9»          &a"+p.getName()+" &6termine 2ème de l'event "+Util.CURRENT_EVENT_NAME);
                    Util.winners.put(2, p);
                    brodcast("");
                    brodcast("&9»          &a"+killer+" &6termine 1er de l'event "+Util.CURRENT_EVENT_NAME);
                    Util.winners.put(1, Bukkit.getPlayer(killer));
                    list.remove(p);
                    break;
            }
            brodcast("");
        }
    }
    public static void announceTop(Player p){
        int size = participant.size();
        if(size<=3)
        {
            brodcast("");
            switch(size)
            {
                case 3:
                    brodcast("&9»          &a"+p.getName()+" &6termine 3ème de l'event "+Util.CURRENT_EVENT_NAME);
                    Util.winners.put(3, p);
                    break;
                case 2:
                    brodcast("&9»          &a"+p.getName()+" &6termine 2ème de l'event "+Util.CURRENT_EVENT_NAME);
                    Util.winners.put(2, p);
                    brodcast("");
                    brodcast("&9»          &a"+participant.get(0)+" &6termine 1er de l'event "+Util.CURRENT_EVENT_NAME);
                    Util.winners.put(1, Bukkit.getPlayer(participant.get(0)));
                    break;
            }
            brodcast("");
        }
    }
    public static int getScoreMaxPage() {
        return (int) Math.ceil((float) getScoreSubCommands().size() / CMD_PER_PAGE);
    }
    public static int getCheckpointMaxPage() {
        return (int) Math.ceil((float) getCheckpointSubCommands().size() / CMD_PER_PAGE);
    }
    public static int getBuildBattleMaxPage(Player p) {
        return (int) Math.ceil((float) getBuildBattleSubCommands(p).size() / CMD_PER_PAGE);
    }
    public static int getMaxPageTuto() {
        return (int) Math.ceil((float) TutoHelper.class.getEnumConstants().length / TUTO_PER_PAGE);
    }
    public static List<String> getSection(){
        List<String> section = new LinkedList<>();

        Map<String, Object> map = config.getValues(false);
        for (Map.Entry<String, Object> e : map.entrySet()) {
            section.add(e.getKey());
        }

        return section;
    }

    public static List<String> getRegion(Player p){
        List<String> region = new LinkedList<>();

        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        for(ProtectedRegion r : container.get(BukkitAdapter.adapt(p.getWorld())).getApplicableRegions(BukkitAdapter.asBlockVector(p.getLocation())))
        {
            region.add(r.getId());
        }
        return region;
    }

}
